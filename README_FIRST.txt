
Pour réaliser ce projet les outils suivants ont été nécessaires :
_framework laravel avec l'outil composer
_installation manuelle de PHP
_l'utilisation du SGBD Sqlite3
_éditeur PhpStorm

Attention: certains dossiers fournis par laravel lors de la création du projet étant lourds, ils ont été supprimés, 
si le projet venait à être testé n'est pas oublier de faire "composer update" pour les faire réaparaitre.