<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    public $timestamps=false;

    protected $hidden=['mdp'];//Le champs mdp n’est pas accessible dans le modèle User
    protected $fillable=['nom','prenom','login'];
    protected $attributes=['type'=>null];

    public function getAuthPassword(){
        return $this->mdp;
    }

    public function isAdmin(){
        return $this->type=='admin';
    }

    public function isEtudiant(){
        return $this->type=='etudiant';
    }

    public function isEnseignant(){
        return $this->type=='enseignant';
    }

    //les relation

    function cours(){
        return $this->belongsToMany(Cours::class,'cours_users');
    }

    function coursUnique(){
        return $this->hasMany(Cours::class);
    }

    function formation(){
        return $this->belongsTo(Formation::class);
    }

}
