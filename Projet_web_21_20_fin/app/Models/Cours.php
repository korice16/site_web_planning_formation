<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cours extends Model
{
    use HasFactory;
    protected $table='cours';

    protected $fillable=['intitule'];

    //les relation

    function users(){
        $this->belongsToMany(User::class,'cours_users');
    }

    function user(){
        return $this->belongsTo(User::class);
    }

    function formation(){
        return $this->belongsTo(Formation::class,);
    }

    function plannings(){
        return $this->hasMany(Planning::class);
    }
}
