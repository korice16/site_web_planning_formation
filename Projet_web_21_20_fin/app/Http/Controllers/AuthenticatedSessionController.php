<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends Controller
{
    /**
     * methode qui demande les information pour se connecter
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function formLogin(){
        return view('compte.login');
    }

    /**
     * methode post qui permet de se connecter
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request)
    {//pour se connecter
        $request->validate([
            'login' => 'required|string|max:30',
            'mdp' => 'required|string:max:60'
        ]);

        $credentials = ['login' => $request->input('login'),
            'password' => $request->input('mdp')];

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            if((AUTH::User()->type==null)){

                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                $request->session()->flash('etat',"Vous n'avez pas encore ete accepter dans par l'ecole, veuillez patienter");
                return redirect('/');
            }
            $request->session()->flash('etat','Login successful');
            return redirect()->intended('/home');
        }

        //il a pas reussi a ce connecter
        return back()->withErrors([
            'login' => 'Erreur survenu sur le login ou le mot de passe',
        ]);

    }

    /**
     * methode qui deconnecte l'utilisateur
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

}
