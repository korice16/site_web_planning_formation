<?php

namespace App\Http\Controllers;

use App\Models\Formation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterUserController extends Controller
{
    public function formRegister(){//page pour s'enregistrer
        $formation=Formation::all();
        return view('compte.register',['formation'=>$formation]);
    }

    public function register(Request $request){
        $request->validate([
            'nom'=>'required|string|max:40',
            'prenom'=>'required|string|max:40',
            'login'=>'required|string|max:30|unique:users',
            'formation_id' => 'required|integer',
            'mdp'=>'required|string|max:60|confirmed'//confirmed: pour confirmer me champs suivant bde mdp
        ]);

        $user=new User();
        $user->nom=$request->nom;
        $user->prenom=$request->prenom;
        $user->login=$request->login;
        $user->mdp=hash::make($request->mdp);//hash::make() pour coder le mdp, le securiser


        if ($request->formation_id==0){
            $user->save();
            $request->session()->flash('etat',"Creation du compte en tant que enseignant effectuer, attendre l'approbation de l'ecole");
            return redirect('/');
        }
        $formation=Formation::findOrFail($request->formation_id);
        $formation->users()->save($user);

        //Auth::login($user);//optionelle: pour connecter directement l(utilisateur
        $request->session()->flash('etat',"Creation du compte en tant que etudiant effectuer, attendre l'approbation de l'ecole");
        return redirect('/');
    }

}
