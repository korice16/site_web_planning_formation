<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CompteController extends Controller
{
    /**
     * page pour la modification du mot de passe
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modifier_mdp_form(){
        return view('compte.modifier_mdp_form');
    }

    /**
     * methode post qui gere la modification du mot de passe
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifier_mdp(Request $request){
        $validated=$request->validate([
            'a_mdp'=>'required|string|max:60',
            'mdp'=>'required|string|max:60|confirmed'
        ]);
        //$m_mdp=hash::make($request->a_mdp);
        $mdp_c=AUTH::User()->mdp;

        if(Hash::check($validated['a_mdp'],$mdp_c)){
            $user_id=Auth::User()->id;
            $user=User::findOrFail($user_id);
            //$user = $user->makeVisible(['mdp']);
            $user->mdp=Hash::make($validated['mdp']);
            //$user->timestamps = false;
            $user->save();
            $request->session()->flash('etat','Modification effectuer');
            return redirect()->route('compte.profile');
        }

        $request->session()->flash('etat','erreur dans votre saisi, veuillez reesayer');
        return redirect()->route('compte.profile.modifier_mdp_form');
    }

    /**
     * page pour modifier le nom ou le prenom
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modifier_nom_prenom_form(){
        return view('compte.modifier_nom_prenom_form',['user'=>User::findOrFail(AUTH::User()->id)]);
    }

    public function modifier_nom_prenom(Request $request){
        $validated=$request->validate([
            'nom'=>'required|string|max:40',
            'prenom'=>'required|string|max:40'
        ]);
        $user=User::findOrFail(AUTH::User()->id);
        $user->nom=$validated['nom'];
        $user->prenom=$validated['prenom'];
        $user->save();


        $request->session()->flash('etat','Modification du nom et prenom effectuer');
        return redirect()->route('compte.profile');
    }
}
