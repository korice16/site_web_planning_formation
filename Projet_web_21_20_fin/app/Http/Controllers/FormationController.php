<?php

namespace App\Http\Controllers;

use App\Models\Cours;
use App\Models\Formation;
use App\Models\Planning;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormationController extends Controller
{
    //Partie administrateur

        //4.3.1. Liste

    /**
     * affiche la liste des formations
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list_formations(){
        $formation=Formation::paginate(5);
        return view('admin.formations.list_formations',['formations'=>$formation]);
    }

        //4.3.2. Ajout d’une formation.

    /**
     * methode post qui permet d'ajouter une formation
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ajout(Request $request){
        $request->validate([
           'formation'=>'required|string|max:50'
        ]);

        $formation=new Formation();
        $formation->intitule=$request->formation;
        $formation->save();
        $request->session()->flash('etat',"ajout de la formation '$request->formation' effectuer");
        return redirect()->route('admin.formations');
    }

        //4.3.3. Modification d’une formation.

    /**
     * methode qui renvoie vers la page qui doit demander les information pour modifier la formation
     * @param $formation_id l'id de la formation à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modification_form($formation_id){
        $formation=Formation::findOrFail($formation_id);

        return view('admin.formations.modification_form',['formation'=>$formation]);

    }

    /**
     * methode post qui modifie la formation
     * @param Request $request
     * @param $formation_id l'id de la formation à modifier
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modification(Request $request,$formation_id){
        $request->validate([
           'intitule'=>'required|string|max:50'
        ]);

        $formation=Formation::findOrFail($formation_id);
        $formation->intitule=$request->intitule;
        $formation->save();

        $request->session()->flash('etat','modification effectuer');
        return redirect()->route('admin.formations');
    }

        //4.3.4. Suppression d’une formation.

    /**
     * methode qui supprimer une formation
     * @param Request $request
     * @param $formation_id l'id de la formation à supprimer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function suppression(Request $request,$formation_id){

        $form_cours=DB::table('cours')->where('formation_id','=',$formation_id)->get();

        foreach ($form_cours as $form_cour){

            DB::delete('delete from cours_users where cours_id=?',[$form_cour->id]);

            /*
            $cours_users=DB::table('cours_users')->where('cours_id','=',$form_cour->id)->get();//les etudiants inscrits dans un cours qui appartiens à la formation à supprimer
            //$cours_users->delete();
            foreach ($cours_users as $cours_use){
              // $cours_users_trouve=DB::table('cours_users')->where('cours_id','=',$form_cour->id)->first();
                //$cours_users_trouve->delete();
                DB::delete('delete from cours_users where cours_id=?',[$form_cour->id]);
                //$cours_use->delete();
            }*/

            DB::delete('delete from plannings where cours_id=?',[$form_cour->id]);
            /*
            $plannings=Planning::where('cours_id','=',$form_cour->id)->get();//tous les seances appartenants aux cours issue de la formation à supprimer
            //$plannings->delete();
            foreach ($plannings as $plan){
                $plan_trouve=Planning::findOrFail($plan->id)->first();
                $plan_trouve->delete();
                //$plan->delete();
            }*/

            $form_cour_trouve=Cours::findOrFail($form_cour->id);
            $form_cour_trouve->delete();
        }

        //DB::update('update users set type=null where formation_id=?',[$formation_id]);

        /*
        $users=User::where('formation_id','=',$formation_id)->get();
        foreach ($users as $use){
            $use->formation_id=null;
            $use->delete();
        }*/

        DB::delete('delete from users where formation_id=?',[$formation_id]);

        $formation=Formation::findOrFail($formation_id);
        $formation->delete();

        $request->session()->flash('etat','Supression effectuer');
        return back();
    }

}
