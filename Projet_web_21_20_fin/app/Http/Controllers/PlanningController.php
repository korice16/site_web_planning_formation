<?php

namespace App\Http\Controllers;

use App\Models\Cours;
use App\Models\Planning;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlanningController extends Controller
{
    //Partie enseignant

    //Gestion du planning :
    /**
     * @param $type
     * @param $user_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home_planning($type,$user_id){
        if($type=='enseignant')
            return view('enseignant.planning.home_planning',['type'=>$type,'user_id'=>$user_id]);
        elseif ($type=='admin')return view('admin.planning.home_planning',['type'=>$type,'user_id'=>$user_id]);
    }

    /**
     * @param $type
     * @param $user_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home_cours_ensei($type,$user_id){
        if($type=='enseignant')
            return view('enseignant.planning.cours.home_cours_ensei',['type'=>$type,'user_id'=>$user_id]);
        elseif ($type=='admin')
            return view('admin.planning.cours.home_cours_ensei',['type'=>$type,'user_id'=>$user_id]);
    }

    //2.3.1. Création d’une nouvelle séance de cours. -----------------PAR COURS----------------
    /**
     * methode de la mise en page pour la creation d'une seance de cours
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function creation_seance_cours_form($type,$user_id){
        $users=User::findOrFail($user_id);
        $cours_r=$users->coursUnique;

        if($type=='enseignant' || $type=='enseignant_v2')
            return view('enseignant.planning.cours.creation_seance_cours_form',['cours_r'=>$cours_r,'type'=>$type,'user_id'=>$user_id]);
        elseif ($type=='admin')
            return view('admin.planning.cours.creation_seance_cours_form',['cours_r'=>$cours_r,'type'=>$type,'user_id'=>$user_id]);
    }

    /**
     * methode post qui creer une seance de cours pour l'enseignant
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function creation_seance_cours(Request $request,$type,$user_id){
        $request->validate([
            'cours_id'=>'required|integer',
            'date_cours'=>'required|date',
            //'date_debut'=>'required|date',
            'time_debut'=>'required',
            //'date_fin'=>'required|date'
            'time_fin'=>'required'
        ]);

        $date_cours=new Carbon($request->date_cours);

        $time_debut=new Carbon($request->time_debut);
        $time_fin=new Carbon($request->time_fin);

        $date_debut=Carbon::create($date_cours->year,$date_cours->month,$date_cours->day,$time_debut->hour,$time_debut->minute);
        $date_fin=Carbon::create($date_cours->year,$date_cours->month,$date_cours->day,$time_fin->hour,$time_fin->minute);

        $cours=Cours::findOrFail($request->cours_id);
        $planning=new Planning();
        $planning->date_debut=$date_debut;
        $planning->date_fin=$date_fin;

        if ($date_debut >= $date_fin) {
            $request->session()->flash('etat', "la date du debut du cours n'est peut pas être superieur ou égale a la date du fin du cours ");
            return back();
        }
        $cours->plannings()->save($planning);

        $request->session()->flash('etat', "ajout de la seance '$cours->intitule' de '$date_debut' à '$date_fin' effectuer");
        if($type=='enseignant')
            return redirect()->route('enseignant.planning.cours.home_cours_ensei', ['type' => $type, 'user_id' => $user_id]);
        elseif($type=='admin')
            return redirect()->route('admin.planning.cours.home_cours_ensei', ['type' => $type, 'user_id' => $user_id]);
        elseif($type=='enseignant_v2')
            return back();
    }

    //2.3.2. Mise à jour d’une séance de cours. =>par cours

    /**
     * methode qui affiche la liste des seances du cours de l'enseignant
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list_maj_seance_cours_form($type,$user_id){
        $users=User::findOrFail($user_id);
        //$planning_r=$users->coursUnique->plannings;

        $cours_respo=$users->coursUnique;
        $planning_r=[];
        $cours_r=[];

        foreach ($cours_respo as $cours){
            $plan_exist=$cours->plannings;
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

            }
        }

        if($type=='enseignant')
            return view('enseignant.planning.cours.list_maj_seance_cours_form',['planning_r'=>$planning_r,'cours'=>$cours_r,'type' => $type, 'user_id' => $user_id]);
        elseif($type=='admin')
            return view('admin.planning.cours.list_maj_seance_cours_form',['planning_r'=>$planning_r,'cours'=>$cours_r,'type' => $type, 'user_id' => $user_id]);
    }

    /**
     * methode qui demande les information necessaire pour modifier une seance de cours
     * @param $planning_id l'id de la seance à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function maj_seance_cours_form($type,$user_id,$planning_id){
        $planning=Planning::findOrFail($planning_id);
        $cours_r=Cours::findOrFail($planning->cours_id);

        $date[0]=$plan_deb=new Carbon($planning->date_debut);
        $date[1]=$plan_fin=new Carbon($planning->date_fin);
        if($type=='enseignant' || $type == 'enseignant_v3')
            return view('enseignant.planning.cours.maj_seance_cours_form',['date'=>$date,'plannings'=>$planning,'cours_r'=>$cours_r,'type' => $type, 'user_id' => $user_id]);
        elseif ($type=='admin')
            return view('admin.planning.cours.maj_seance_cours_form',['date'=>$date,'plannings'=>$planning,'cours_r'=>$cours_r,'type' => $type, 'user_id' => $user_id]);
    }


    /**
     * methode post qui modifie une seance de cours
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function maj_seance_cours(Request $request,$type,$user_id)
    {
        $request->validate([
            'planning_id' => 'required|integer',
            'date_cours'=>'required|date',
            'time_debut_nv'=>'required',
            'time_fin_nv'=>'required'
        ]);

        $date_cours=new Carbon($request->date_cours);

        $time_debut=new Carbon($request->time_debut_nv);
        $time_fin=new Carbon($request->time_fin_nv);

        $date_debut=Carbon::create($date_cours->year,$date_cours->month,$date_cours->day,$time_debut->hour,$time_debut->minute);
        $date_fin=Carbon::create($date_cours->year,$date_cours->month,$date_cours->day,$time_fin->hour,$time_fin->minute);

            $carbon_debut_nv = new Carbon($request->date_debut_nv);
            $carbon_fin_nv = new Carbon($request->date_fin_nv);
            if ($date_debut >= $date_fin) {
                $request->session()->flash('etat', "la date du debut du cours n'est peut pas être superieur ou egale a la date du fin du cours");
                return back();
            }

            $planning = Planning::findOrFail($request->planning_id);

            if(($planning->date_debut==$date_debut) && ($planning->date_fin==$date_fin)){
                $request->session()->flash('etat', "aucune modification effectuer sur le cours");
                return back();
            }
            $planning->date_debut = $date_debut;
            $planning->date_fin = $date_fin;
            $planning->save();
            $request->session()->flash('etat', 'modification de la seance du cours effectuer');
            if ($type == 'enseignant')
                return redirect()->route('enseignant.planning.cours.list_maj_seance_cours_form',['type' => $type, 'user_id' => $user_id]);
            elseif ($type=='admin')
                return redirect()->route('admin.planning.cours.list_maj_seance_cours_form',['type' => $type, 'user_id' => $user_id]);
            elseif ($type == 'enseignant_v3') return back();

    }

    //2.3.3. Suppression d’une séance de cours =>par cours

    /**
     * methode qui permete la supression d'une seance dans le planning
     * @param Request $request
     * @param $planning_id l'id de la seance dans le planning
     * @return \Illuminate\Http\RedirectResponse
     */
    public function supprimer_seance_cours(Request $request,$type,$user_id,$planning_id){

        $planning=Planning::findOrFail($planning_id);
        $planning->delete();


            $request->session()->flash('etat', 'Suppression du cours effectuer');
            return back();

    }

    //2.3.1. Création d’une nouvelle séance de cours. -------PAR SEMAINE-------------------

    /**
     *
     * @param $type
     * @param $user_id l'id de l'utilisateur à qui le planning av être gerer
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function home_par_semaine_ensei($type,$user_id){
        if($type=='enseignant')
            return view('enseignant.planning.semaine.home_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id]);
        elseif ($type=='admin')
            return view('admin.planning.semaine.home_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id]);
    }

    /**
     * affcihe les cours de la semaine actuelle
     * @param $type
     * @param $user_id l'id de l'enseignant dont le cours va etre afficher
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_act_v2_ensei($type,$user_id){
        $startweek=Carbon::now()->startOfWeek();
        $endweek=Carbon::now()->endOfWeek();

        $users=User::findOrfail($user_id);
        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_respo as $cours){
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek,$endweek])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

            }
        }


        if($type=='enseignant')
            return view('enseignant.planning.semaine.affichage_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek->format('Y-m-d'),'et'=>$endweek->format('Y-m-d')]);
        elseif($type=='admin')
            return view('admin.planning.semaine.affichage_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek->format('Y-m-d'),'et'=>$endweek->format('Y-m-d')]);
    }

    /**
     * affcihe les cours de la semaine selon les dates
     * @param $type
     * @param $user_id l'id de l'enseignant dont le cours va etre afficher
     * @param $page 'p' precedent, 's' suivant
     * @param $startweek la date du debut debut de la semaine (lundi)
     * @param $endweek la date de fin de la semaine (dimanche)
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_v2_ensei($type,$user_id,$page,$startweek,$endweek){
        $startweek_new=new Carbon($startweek);
        $endweek_new=new Carbon($endweek);
        if($page=='s'){
            $startweek_new=(new Carbon($startweek))->addWeek();
            $endweek_new=(new Carbon($endweek))->addWeek();
        }else if($page=='p'){
            $startweek_new=(new Carbon($startweek))->subWeek();
            $endweek_new=(new Carbon($endweek))->subWeek();
        }

        $users=User::findOrFail($user_id);
        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];
        $date=[];

        foreach ($cours_respo as $cours){
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek_new,$endweek_new])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $date[$plan_e->id*2]=new Carbon($plan_e->date_debut);
                $date[($plan_e->id*2)+1]=new Carbon($plan_e->date_fin);
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])){
                    $cours_r[$plan_e->cours_id]=$cours_trouve;

                }
            }
        }
/*
        $jours=[];
        //$st=new Carbon($startweek_new);
        for ($a=0;$a<7;$a++){
            $st=new Carbon($startweek_new);
            //$jours[$a]=(new Carbon($startweek_new))->addDays($a);
            $jours[$a]=$st->addDays($a+1);
        }

        $planning_jour=[[]];

        foreach ($planning_r as $plan){
            if($plan->date_debut < $jours[0]) $planning_jour[0][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[0])&&($plan->date_debut < $jours[1])) $planning_jour[1][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[1])&&($plan->date_debut < $jours[2])) $planning_jour[2][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[2])&&($plan->date_debut < $jours[3])) $planning_jour[3][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[3])&&($plan->date_debut < $jours[4])) $planning_jour[4][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[4])&&($plan->date_debut < $jours[5])) $planning_jour[5][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[5])&&($plan->date_debut < $jours[6])) $planning_jour[6][$plan->id]=$plan;
            elseif (($plan->date_debut > $jours[6])&&($plan->date_debut < $jours[7])) $planning_jour[7][$plan->id]=$plan;
        }
*/
        if($type=='enseignant')
            return view('enseignant.planning.semaine.affichage_par_semaine_ensei',[/*'planning_jour'=>$planning_jour,'jours'=>$jours,'date'=>$date,*/'type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek_new->format('Y-m-d'),'et'=>$endweek_new->format('Y-m-d')]);
        elseif ($type=='admin')
            return view('admin.planning.semaine.affichage_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek_new->format('Y-m-d'),'et'=>$endweek_new->format('Y-m-d')]);
    }

        //2.3.2. Mise à jour d’une séance de cours. 2.3.3. Suppression d’une séance de cours.

    /**
     * affcihe les cours de la semaine actuelle
     * @param $type
     * @param $user_id l'id de l'enseignant dont le cours va etre afficher
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_act_v3_ensei($type,$user_id){
        $startweek=Carbon::now()->startOfWeek();
        $endweek=Carbon::now()->endOfWeek();

        $users=User::findOrfail($user_id);
        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_respo as $cours){
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek,$endweek])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

            }
        }

        if($type=='enseignant')
            return view('enseignant.planning.semaine.affichage_par_semaine_v3_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek->format('Y-m-d'),'et'=>$endweek->format('Y-m-d')]);
        elseif($type=='admin')
            return view('admin.planning.semaine.affichage_par_semaine_v3_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek->format('Y-m-d'),'et'=>$endweek->format('Y-m-d')]);
    }


    /**
     * affcihe les cours de la semaine selon les dates
     * @param $type
     * @param $user_id l'id de l'enseignant dont le cours va etre afficher
     * @param $page 'p' precedent, 's' suivant
     * @param $startweek la date du debut debut de la semaine (lundi)
     * @param $endweek la date de fin de la semaine (dimanche)
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_v3_ensei($type,$user_id,$page,$startweek,$endweek){
        $startweek_new=new Carbon($startweek);
        $endweek_new=new Carbon($endweek);
        if($page=='s'){
            $startweek_new=(new Carbon($startweek))->addWeek();
            $endweek_new=(new Carbon($endweek))->addWeek();
        }else if($page=='p'){
            $startweek_new=(new Carbon($startweek))->subWeek();
            $endweek_new=(new Carbon($endweek))->subWeek();
        }

        $users=User::findOrfail($user_id);
        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_respo as $cours){
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek_new,$endweek_new])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

            }
        }
        if($type=='enseignant')
            return view('enseignant.planning.semaine.affichage_par_semaine_v3_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek_new->format('Y-m-d'),'et'=>$endweek_new->format('Y-m-d')]);
        elseif ($type=='admin')
            return view('admin.planning.semaine.affichage_par_semaine_v3_ensei',['type'=>$type,'user_id'=>$user_id,'planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek_new->format('Y-m-d'),'et'=>$endweek_new->format('Y-m-d')]);
    }

    //2.2. Voir le planning personnalisé (les séances dont on est responsable) :

    //2.2.1. Intégral.
    /**
     * mehtode qui affiche tous les seances dont l'enseignant est responsable
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_integrale_ensei(){
        $users=Auth::User();
        //$planning_r=$users->coursUnique->plannings;

        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];
        if(count($cours_respo)!=0){
            foreach ($cours_respo as $cours){
                $plan_exist=$cours->plannings;
                if(count($plan_exist)!=0){
                    foreach ($plan_exist as $plan_e){
                        $planning_r[$plan_e->id]=$plan_e;
                        $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                        if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

                    }
                }
            }
        }

        return view('enseignant.planning_perso.affichage_integrale_ensei',['planning_r'=>$planning_r,'cours'=>$cours_r]);
    }

        //2.2.2. Par cours.

    /**
     * methode qui affiche tous les seances d'un cours
     * @param $cours_id l'id du cours dont on doit afficher els seances
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_cours_ensei($cours_id){
        $cours=Cours::findOrFail($cours_id);
        $planning=Planning::where('cours_id','=',$cours_id)->get();

        return view('enseignant.planning_perso.affichage_par_cours_ensei',['cours'=>$cours,'planning'=>$planning]);
    }

        //2.2.3. Par semaine.

    /**
     * affichage des cours pour la semaine actuelle de l'etudiant
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_act_ensei($user_id){
        $startweek=Carbon::now()->startOfWeek();
        $endweek=Carbon::now()->endOfWeek();

        //$users=AUTH::user();
        $users=User::findOrFail($user_id);
        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_respo as $cours){
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek,$endweek])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

            }
        }
            return view('enseignant.planning_perso.affichage_par_semaine_ensei',['planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek->format('Y-m-d'),'et'=>$endweek->format('Y-m-d')]);
    }

    /**
     * affichage des cours pour n'importe qu'elle semaine de l'etudiant
     * @param $type le type 's' pr la semaine suivante et 'p' pr la semaine precedante
     * @param $startweek le debut de la semaine
     * @param $endweek la fin de la semaine
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_ensei($user_id,$type,$startweek,$endweek){
        $startweek_new=new Carbon($startweek);
        $endweek_new=new Carbon($endweek);
        if($type=='s'){
            $startweek_new=(new Carbon($startweek))->addWeek();
            $endweek_new=(new Carbon($endweek))->addWeek();
        }else if($type=='p'){
            $startweek_new=(new Carbon($startweek))->subWeek();
            $endweek_new=(new Carbon($endweek))->subWeek();
        }

       //$user=(AUTH::user())->id;
        $users=User::findOrFail($user_id);
        $cours_respo=$users->coursUnique;

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_respo as $cours){
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek_new,$endweek_new])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;

            }
        }

            return view('enseignant.planning_perso.affichage_par_semaine_ensei',['planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek_new->format('Y-m-d'),'et'=>$endweek_new->format('Y-m-d')]);

    }

    //======================================================================================================================================

    //Partie etudiant

    //1.3. Affichage du planning personnalisé (uniquement les séances des cours auxquels cet étudiant est inscrit) :

    //1.3.1. Intégral.

    /**
     * methode qui permet l'affichage integrales des seances de cours auxquels l'etudiant est inscrit
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_integrale_etu(){
        $users=AUTH::User();
        $cours_deja=DB::table('cours_users')->where('user_id','=',$users->id)->get();
        $cours_inscrit=[];

            foreach ($cours_deja as $cour){
                $cours_t=Cours::findOrFail($cour->cours_id);
                if(empty($cours_inscrit[$cour->cours_id])) $cours_inscrit[$cour->cours_id]=$cours_t;
            }

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_inscrit as $cours){
            $plan_exist=$cours->plannings;
                foreach ($plan_exist as $plan_e){
                    $planning_r[$plan_e->id]=$plan_e;
                    $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                    if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;
                }
        }

        return view('etudiant.planning_perso.affichage_integrale_etu',['planning_e'=>$planning_r,'cours_e'=>$cours_r]);
    }

    //1.3.2. Par cours.
    /**
     * methode qui affiche tous les seances d'un cours
     * @see CoursController/list_cours_formation()
     * @param $cours_id l'id du cours dont on doit afficher els seances
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_cours_etu($cours_id){
        $cours=Cours::findOrFail($cours_id);
        $planning=Planning::where('cours_id','=',$cours_id)->get();

        return view('etudiant.planning_perso.affichage_par_cours_etu',['cours'=>$cours,'planning'=>$planning]);
    }

    //1.3.3. Par semaine.

    /**
     * affichage des cours pour la semaine actuelle de l'etudiant
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_act_etu(){
        $startweek=Carbon::now()->startOfWeek();
        $endweek=Carbon::now()->endOfWeek();
        $users=AUTH::User();
        $cours_deja=DB::table('cours_users')->where('user_id','=',$users->id)->get();

        $cours_inscrit=[];

        foreach ($cours_deja as $cour){
            $cours_t=Cours::findOrFail($cour->cours_id);
            if(empty($cours_inscrit[$cour->cours_id])) $cours_inscrit[$cour->cours_id]=$cours_t;
        }

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_inscrit as $cours){
            //$plan_exist=($cours->plannings)->whereBetween('date_debut',[$startweek,$endweek])->get();
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek,$endweek])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;
            }
        }

        return view('etudiant.planning_perso.affichage_par_semaine_etu',['planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek->format('Y-m-d'),'et'=>$endweek->format('Y-m-d')]);
    }

    /**
     * affichage des cours pour n'importe qu'elle semaine de l'etudiant
     * @param $type le type 's' pr la semaine suivante et 'p' pr la semaine precedante
     * @param $startweek le debut de la semaine
     * @param $endweek la fin de la semaine
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function affichage_par_semaine_etu($type,$startweek,$endweek){
        $startweek_new=new Carbon($startweek);
        $endweek_new=new Carbon($endweek);
        if($type=='s'){
            $startweek_new=(new Carbon($startweek))->addWeek();
            $endweek_new=(new Carbon($endweek))->addWeek();
        }else if($type=='p'){
            $startweek_new=(new Carbon($startweek))->subWeek();
            $endweek_new=(new Carbon($endweek))->subWeek();
        }

        $users=AUTH::User();
        $cours_deja=DB::table('cours_users')->where('user_id','=',$users->id)->get();

        $cours_inscrit=[];

        foreach ($cours_deja as $cour){
            $cours_t=Cours::findOrFail($cour->cours_id);
            if(empty($cours_inscrit[$cour->cours_id])) $cours_inscrit[$cour->cours_id]=$cours_t;
        }

        $planning_r=[];
        $cours_r=[];

        foreach ($cours_inscrit as $cours){
            //$plan_exist=($cours->plannings)->whereBetween('date_debut',[$startweek,$endweek])->get();
            $plan_exist=Planning::where('cours_id','=',$cours->id)->whereBetween('date_debut',[$startweek_new,$endweek_new])->get();
            foreach ($plan_exist as $plan_e){
                $planning_r[$plan_e->id]=$plan_e;
                $cours_trouve=Cours::findOrFail($plan_e->cours_id);
                if(empty($cours_r[$plan_e->cours_id])) $cours_r[$plan_e->cours_id]=$cours_trouve;
            }
        }

        return view('etudiant.planning_perso.affichage_par_semaine_etu',['planning_e'=>$planning_r,'cours_e'=>$cours_r,'st'=>$startweek_new->format('Y-m-d'),'et'=>$endweek_new->format('Y-m-d')]);
    }
}
