<?php

namespace App\Http\Controllers;

use App\Models\Cours;
use App\Models\Formation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Table;

class CoursController extends Controller
{
    //Partie admin

    //Partie 4.2    Gestion des cours
        //4.2.1 Liste

    /**
     * affiche la liste de tous les cours
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function cours_list(){
        $cours=Cours::all();
        $cours_nom=[];
        foreach ($cours as $cour){
            if(empty($cours_nom[$cour->id])) {
                $teach=User::findOrFail($cour->user_id);
                $cours_nom[$cour->user_id]=$teach->login;
            }
        }
        return view('admin.cours.cours_list',['cours'=>$cours,'cours_nom'=>$cours_nom]);
    }

        //4.2.3. Création d’un cours.

    /**
     * page qui demande les information pour la creation d'un cours
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function creation_form(){
        $enseignant=User::where('type','=','enseignant')->get();
        $formation=Formation::all();
        return view('admin.cours.creation_form',['enseignant'=>$enseignant,'formation'=>$formation]);
    }

    /**
     * methode post qui gere la creation d'un cours
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function creation(Request $request){
        $request->validate([
            'intitule'=>'required|string|max:50',
            'formation_id'=>'required|integer',
            'enseignant_id'=>'required|integer'
        ]);

        $teacher=User::findOrFail($request->enseignant_id);
        $teacher_cours=$teacher->coursUnique;

        if(!empty($teacher_cours)){//va verifier si l'utulistauers est deja assigner a ce cours
            foreach ($teacher_cours as $cou){
                if($request->intitule==$cou->intitule) {
                    $cours_form = $cou->formation;
                    if (!empty($cours_form)&&($cours_form==$request->formation_id)){
                        $request->session()->flash('etat', "L'enseignant '$teacher->login' est deja assigner au cours '$request->intitule'");
                        //return redirect()->route('');
                        return back();
                    }
                }
            }
        }

        $cours=new Cours();
        $cours->intitule=$request->intitule;
        $teacher->coursUnique()->save($cours);

        $formation=Formation::findOrFail($request->formation_id);
        $formation->cours()->save($cours);

        $request->session()->flash('etat',"Ajout du cours '$cours->intitule' pour l'enseignant '$teacher->login' effectuer ");
        return back();
        //return redirect()->route('admin.cours.cours_list');
    }

        //4.2.4. Modification d’un cours.

    /**
     * mehtode qui renvoie vers la vue qui demande les information necessaire pour modifier un cours
     * @param Request $request
     * @param $cours_id l'id du cours à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modification_cours_form(Request $request,$cours_id){
        $cours=Cours::findOrFail($cours_id);
        $teachers=User::where('type','=','enseignant')->get();
        $teacher_act=User::findOrFail($cours->user_id);
        $formation=Formation::all();
        $forma_act=Formation::findOrFail($cours->formation_id);
        return view('admin.cours.modification_cours_form',['cours'=>$cours,'teachers'=>$teachers,'formation'=>$formation,'teacher_act'=>$teacher_act,'forma_act'=>$forma_act]);
    }

    /**
     * methoe post qui modifie un cours
     * @param Request $request
     * @param $cours_id l'id du cours à modifier
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modification_cours(Request $request,$cours_id){
        $request->validate([
           'intitule'=>'required|string|max:50',
            'user_id'=>'required|integer',
            'formation_id'=>'required|integer'
        ]);

        $cours=Cours::findOrFail($cours_id);
        if(($cours->intitule==$request->intitule) &&($cours->user_id==$request->user_id) && ($cours->formation_id==$request->formation_id)){
            $request->session()->flash('etat','Aucune modification');
            return back();
        }

        $cours->intitule=$request->intitule;
        $cours->user_id=$request->user_id;
        $cours->formation_id=$request->formation_id;
        $cours->save();

        $request->session()->flash('etat',"modification du cours '$cours->intitule' effectuer");
        return redirect()->route('admin.cours.cours_list');
    }

        //4.2.5. Suppression d’un cours.

    /**
     * methode qui supprime un cours
     * @param Request $request
     * @param $cours_id l'id du cours à supprimer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function suppression_cours(Request $request,$cours_id){

        $form_cours=Cours::findOrFail($cours_id);

        DB::delete('delete from cours_users where cours_id=?',[$form_cours->id]);

        DB::delete('delete from plannings where cours_id=?',[$form_cours->id]);

        $form_cours->delete();

        $request->session()->flash('etat',"Suppression du cours '$form_cours->intitule' effectuer");
        return back();
    }

        //4.2.7. Liste des cours par enseignant (similaire à 2.1, mais pour n’importe quel enseignant).

    public function list_cours_enseignant($user_id){
        $user=User::findOrFail($user_id);

        $cours_r=(User::findOrFail($user_id))->coursUnique;

        $formation=[];
        foreach ($cours_r as $cours){
            $formation[$cours->formation_id]=Formation::findOrFail($cours->formation_id);
        }

        return view('admin.cours.list_cours_enseignant',['cours_r'=>$cours_r,'teacher'=>$user,'formation'=>$formation]);
    }
//=================================================================================================================================================

    //1. Pour les étudiants :
        //1.1. Voir la liste des cours de la formation (dans laquelle l’étudiant est inscrit).

    /**
     * methode qui affiche les cours de la formation de l'etudiant qui est connectée
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list_cours_formation($but){
        $formation=Formation::findOrFail(Auth::User()->formation_id);
        $cours=$formation->cours;//->paginate(5);

        if($but=='list') return view('etudiant.list_cours_formation',['cours'=>$cours,'forma_nom'=>$formation->intitule]);
        return view('etudiant.inscriptions.rechercher_cours_formation',['cours'=>$cours,'forma_nom'=>$formation->intitule]);

    }

        //1.2.1. Inscription dans un cours
    /**
     * methode de la mise en page des informations neccessaire à l'inscription à un cours pour ml'etudiant
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function inscritpion_form(){
        $formation=Formation::findOrFail(Auth::User()->formation_id);
        $cours=$formation->cours;

        return view('etudiant.inscriptions.inscription_form',['cours'=>$cours]);
    }

    /**
     * Methode post qui permet à l'etudiant de s'inscrire à un cours
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inscription(Request $request){
        $request->validate([
           'cours_id'=>'required|integer'
        ]);

        $cours=Cours::findOrFail($request->cours_id);
        $users=AUTH::User();

        $cours_deja=DB::table('cours_users')->where('cours_id','=',$request->cours_id)
            ->where('user_id','=',$users->id)->get();

        //if(empty($cours_deja->cours_id) && empty($cours_deja->user_id)){
        if(count($cours_deja)==0){
            //$users->cours()->attach($cours);
            DB::insert('insert into cours_users (cours_id,user_id) values (?,?)', [$request->cours_id,$users->id]);
            $request->session()->flash('etat',"Inscription au cours '$cours->intitule' effectuer");
            return back();
        }

        $request->session()->flash('etat',"Inscription au cours '$cours->intitule' déjà effectuer");
        return back();
    }

        //1.2.2. Désinscription d’un cours.

    /**
     * methode pour supprimer un cours
     * @param Request $request
     * @param $cours_id l'id du cours à supprimer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function desincription(Request $request,$cours_id){
        $users=AUTH::User();
        DB::delete('delete from cours_users where (cours_id,user_id)=(?,?)',[$cours_id,$users->id]);

        $cours=Cours::findOrFail($cours_id);
        $request->session()->flash('etat',"Desinscription du cours '$cours->intitule' effectuer");
        return back();
    }

        //1.2.3. Liste des cours auxquels l’étudiant est inscrit.
    /**
     * methode qui affiche les cours auxquels l'etudiant est inscrit
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list_cours_inscrit(Request $request,$type){
        $users=AUTH::User();
        $cours_deja=DB::table('cours_users')->where('user_id','=',$users->id)->get();
        $cours=[];

        if($cours_deja!=array()){
            foreach ($cours_deja as $cour){
                $cours_t=Cours::findOrFail($cour->cours_id);
                $cours[$cour->cours_id]=$cours_t;
            }
        }

        //$cours_inscrit=$users->cours()->where('user_id','=',$users->id)->get();//quand j'utilise ca, pour recuperer les cours de l'etudiant
        if($type=='gestion_inscription') {
            return view('etudiant.inscriptions.list_cours_inscrit', ['cours_deja' => $cours_deja, 'cours_tab' => $cours]);
        }else if($type='planning_perso_etu'){
            return view('etudiant.planning_perso.list_cours_inscrit', ['cours_deja' => $cours_deja, 'cours_tab' => $cours]);
        }else{
            $request->session()->flash('etat','erreur dans la redirection concu par le concepteur');
            return back();
        }
    }

    //============================================================================================================================================

    //2. Pour les enseignants :

        //2.1. Voir la liste des cours dont on est responsable.

    /**
     * methode qui affcihe la liste des cours de l'enseignant
     * @param Request $request
     * @param $type selon la redirection de la route
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function list_cours_responsable(Request $request,$type){

        $cours_r=(Auth::User())->coursUnique;

        if($type=='list_cours_responsable'){
            return view('enseignant.list_cours_responsable',['cours_r'=>$cours_r]);
        }else if($type=='planning_perso_ensei'){
            return view('enseignant.planning_perso.list_cours_responsable',['cours_r'=>$cours_r]);
        }else{
            $request->session()->flash('etat','erreur dans la redirection concu par le concepteur');
            return back();
        }
    }

}
