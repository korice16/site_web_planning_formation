<?php

namespace App\Http\Controllers;


use App\Models\Cours;
use App\Models\Formation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //Partie admin gestion des utilisateur

            //4.1.2 Acceptation (ou refus) d’un utilisateur qui a été auto-crée.
    /**
     * page avec pagination des utilisateurs en attente d'acceptation ou de refus
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function acceptation_refus_form(){
        $user=User::where('type','=',null)->paginate(10);
        $form=[];
        foreach ($user as $use){
            if(isset($use->formation_id)) {
                $formation = Formation::findOrFail($use->formation_id);
                $form[$use->id] = $formation->intitule;
            }
        }
        return view('admin.users.acceptation_refus_form',['user'=>$user,'formation'=>$form]);
    }

    /**
     * methode qui se charge d'accepeter les utilisateurs apres consentence de l'admin
     * @param Request $request
     * @param $user_id l'id de l'utilisateur a accepeter ou refuser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptation(Request $request,$user_id){

        $user=User::findOrFail($user_id);
        if(!empty($user->formation_id)){//si la formation existe donc etudiant
            $user->type='etudiant';
            $user->save();
            $request->session()->flash('etat',"Acceptation de l'utilisateur '$user->login' en tant que etudiant effectuer");
            return redirect()->route('admin.users.acceptation_refus_form');
        }

        $user->type='enseignant';
        $user->save();
        $request->session()->flash('etat',"Acceptation de l'utilisateur '$user->login' en tant que enseignant effectuer");
        return redirect()->route('admin.users.acceptation_refus_form');

    }

    /**
     * refuse l'utilisateur
     * @param Request $request
     * @param $user_id l'id de l'utilisateur a accepeter ou refuser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function refus(Request $request,$user_id){
        $user=User::findOrFail($user_id);
        $user->formation()->dissociate();
        $user->delete();
        $request->session()->flash('etat',"Refus de l'utilisateur '$user->login' effectuer");
        return back();
    }

            //4.1.3 association d'un enseignant a un cours

    /**
     * page pour selectionner un enseignant a associer a un cours
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list_enseignant($definir){
        $enseignant=User::where('type','=','enseignant')->paginate(5);
        return view('admin.users.list_enseignant',['enseignants'=>$enseignant,'definir'=>$definir]);
    }

    /**
     * page qui affiche la liste des cours a associer a l'enseignant
     * @param $teacher_id l'id de l'enseignant dont le cours doit etre associer
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function associer_enseignant_cours_form($definir,$teacher_id){
        $teacher=User::findOrFail($teacher_id);
        $cours=Cours::all();
        $cours_nom=[];
        foreach ($cours as $cour){
            if(empty($cours_nom[$cour->id])) {
                $teach=User::findOrFail($cour->user_id);
                $cours_nom[$cour->user_id]=$teach->login;
            }
        }

        return view('admin.users.associer_enseignant_cours',['teacher'=>$teacher,'cours'=>$cours,'cours_nom'=>$cours_nom,'definir'=>$definir]);
    }

    /**
     * methode post qui associe le cours a l'enseignant
     * @param Request $request
     * @param $teacher_id l'id de l'enseignant dont le cours doit etre associer
     * @param $cours_id l'id du cours a associer a l'enseignany
     * @return \Illuminate\Http\RedirectResponse
     */
    public function associer_enseignant_cours(Request $request,$definir,$teacher_id,$cours_id){

        $teacher=User::findOrFail($teacher_id);
        $cours=Cours::findOrFail($cours_id);

        $teacher->coursUnique()->save($cours);
        $request->session()->flash('etat',"Changement d'enseignant effectuer");
        return redirect()->route('admin.users.list_enseignant',['definir'=>$definir]);
        //return back();
    }

        //4.1.4. Création d’un utilisateur.
    public function creation_users_form(){
        $formation=Formation::all();
        return view('admin.users.creation_users_form',['formation'=>$formation]);
    }

    /**
     * methode post qui permet de creer un utilisateurs
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function creation_users(Request $request){
        $request->validate([
            'nom'=>'required|string|max:40',
            'prenom'=>'required|string|max:40',
            'login'=>'required|string|max:30|unique:users',
            'formation_id' => 'required|integer',
            'mdp'=>'required|string|max:60|confirmed'//confirmed: pour confirmer me champs suivant bde mdp
        ]);

        $user=new User();
        $user->nom=$request->nom;
        $user->prenom=$request->prenom;
        $user->login=$request->login;
        $user->mdp=hash::make($request->mdp);//hash::make() pour coder le mdp, le securiser

        if ($request->formation_id==-1){
            $user->type='admin';
            $user->save();
            $request->session()->flash('etat',"Creation du compte pour l'admin à été effectuer");
            return redirect()->route('admin.users');
        }

        if ($request->formation_id==0){
            $user->type='enseignant';
            $user->save();
            $request->session()->flash('etat',"Creation du compte pour l'enseignant à été effectuer");
            return redirect()->route('admin.users');
        }
        $user->type='etudiant';
        $formation=Formation::findOrFail($request->formation_id);
        $formation->users()->save($user);

        //Auth::login($user);//optionelle: pour connecter directement l(utilisateur
        $request->session()->flash('etat',"Creation du compte pour l'étudiant à été effectuer");
        return redirect()->route('admin.users');
    }

        //4.1.5. Modification d’un utilisateur (y compris le type).

    /**
     * affiche la liste des tous les utilisateurs
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list_users_integrale(){
        $users=User::paginate(10);

        return view('admin.users.modifier_user.list_users_integrale',['users'=>$users]);
    }

    /**
     * le methode qui permet de choisir ce dont on veut modifier de l'utilisateur
     * @param $user_id l'id de l'utilisateur à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modifier_user_home($user_id){

        $user=User::findOrFail($user_id);
        return view('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id,'user'=>$user]);
    }

             //4.1.5 :modifier login,nom,prenom

    /**
     * methode qui demande les information necessaire pour modifier le nom et/ou prenom et/ou login
     * @param $user_id l'id de l'utilisateur à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modifier_n_p_l_form($user_id){
        $user=User::findOrFail($user_id);
        return view('admin.users.modifier_user.modifier_n_p_l_form',['user_id'=>$user_id,'user'=>$user]);
    }

    /**
     * methode post qui modifie le nom,prenom,login de l'utilisateur
     * @param Request $request
     * @param $user_id l'id de l'utilisateur à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modifier_n_p_l(Request $request,$user_id){
        $request->validate([
            'nom'=>'required|string|max:40',
            'prenom'=>'required|string|max:40',
            'login'=>'required|string|max:30|'
        ]);
        $user=User::findOrFail($user_id);

        if(($request->nom==$user->nom)&&($request->prenom==$user->prenom)&&($request->login==$user->login)){
            $request->session()->flash('etat',"Aucune modification effectuer");
            return view('admin.users.modifier_user.modifier_user_home',['user'=>$user,'user_id'=>$user_id]);
        }
        if($request->nom!=$user->nom) $user->nom=$request->nom;
        if($request->prenom!=$user->prenom) $user->prenom=$request->prenom;
        if($request->login!=$user->login) $user->login=$request->login;
        $user->save();

        $request->session()->flash('etat',"Modification de l'utiisateur effectuer");
        return view('admin.users.modifier_user.modifier_user_home',['user'=>$user,'user_id'=>$user_id]);
    }

            // //4.1.5: modifier mdp
    /**
     * methode qui demande les information necessaire pour modifier le mdp
     * @param $user_id l'id de l'utilisateur à modifier
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modifier_mdp_user_form($user_id){
        $user=User::findOrFail($user_id);
        return view('admin.users.modifier_user.modifier_mdp_user_form',['user'=>$user,'user_id'=>$user_id]);
    }

    /**
     * methode post qui modifie l'utilisateur le mdp de l'utilisateur
     * @param Request $request
     * @param $user_id l'id de l'utilisateur à modifier
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifier_mdp_user(Request $request,$user_id){
        $request->validate([
            'mdp'=>'required|string|max:60|confirmed'
        ]);

        $user=User::findOrFail($user_id);
        $user->mdp=Hash::make($request->mdp);
        $user->save();
        $request->session()->flash('etat','Modification du mot de passe effectuer');

        return redirect()->route('admin.users.modifier_user.modifier_user_home',['user'=>$user,'user_id'=>$user_id]);
    }

            //4.1.5: modifier formation/type

    /**
     * methode qui demande les information necessaire pour modifier un utilisateur
     * @param $user_id l'id de l'utilisateur à modifier
     */
    public function modifier_formation_type_form($user_id){
        $user=User::findOrFail($user_id);
        $formation=Formation::all();
        if($user->type=='etudiant'){
            $formation_user=Formation::findOrFail($user->formation_id);
            return view('admin.users.modifier_user.modifier_formation_type_form',['user'=>$user,'user_id'=>$user_id,'formation'=>$formation,'formation_user'=>$formation_user]);
        }

        return view('admin.users.modifier_user.modifier_formation_type_form',['user'=>$user,'user_id'=>$user_id,'formation'=>$formation]);
    }

    /**
     * methode post qui modifie le type et la formation de l'utilisateur
     * @param Request $request
     * @param $user_id l'id de l'utilisateur à modifier
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modifier_formation_type(Request $request,$user_id){
        $request->validate([
            'formation_id' => 'required|integer',
            'type'=>'required|string|max:10'
        ]);

        $user=User::findOrFail($user_id);

        if(($request->formation_id==0) && ($request->type=='enseignant')){//il veut changer l'utilisateur en enseignant
            if(($user->type=='enseignant') && ($user->formation_id==null)){
                $request->session()->flash('etat',"Aucune modification car l'utilisateur est dèjà un enseignant");
                return back();
            }
            if($user->type=='etudiant'){ //modifier un etudiant en enseignant

                DB::delete('delete from cours_users where user_id=?',[$user->id]);
                //$user->cours->detach();
                $user->formation()->dissociate();
                $user->type='enseignant';
                $user->save();

                $request->session()->flash('etat',"Modification de l'utilisateur etudiant en enseignant effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);
            }

            if($user->type=='admin'){//modifier un administrateur en enseignant
                $user->type='enseignant';
                $user->save();

                $request->session()->flash('etat',"Modification de l'utilisateur administrateur en enseignant effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);
            }


        }elseif (($request->formation_id>0) && ($request->type=='etudiant')){//il veut changer l'utilisateur en etudiant
            if(($user->type=='etudiant') && ($request->formation_id==$user->formation_id)){
                $request->session()->flash('etat',"Aucune modification car l'etudiant n'a subi aucun changement");
                return back();
            }

            if(($user->type=='etudiant') && ($request->formation_id!=$user->formation_id)){//modifier la formation d'un etudiant
                //$form=Formation::findOrFail($user->formation_id);
                DB::delete('delete from cours_users where user_id=?',[$user->id]);
                //$user->cours->detach();
                $user->formation()->dissociate();
                $formation=Formation::findOrFail($request->formation_id);
                $formation->users()->save($user);

                $request->session()->flash('etat',"Modification de la formation de l'etudiant de  en '$formation->intitule' effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);
            }

            if($user->type=='enseignant'){// modifier un enseignant en etudiant
                $cours_r=$user->coursUnique;

                foreach ($cours_r as $cour) {
                    //$form_cours = Cours::findOrFail($cour->id);

                    DB::delete('delete from cours_users where cours_id=?', [$cour->id]);

                    DB::delete('delete from plannings where cours_id=?', [$cour->id]);

                    $cour->delete();
                }

                $user->type='etudiant';
                $formation=Formation::findOrFail($request->formation_id);
                $formation->users()->save($user);

                $request->session()->flash('etat',"Modification de l'utilisateur enseignant '$user->login' en etudiant effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);
            }

            if($user->type=='admin'){// modifier un administrateur en etudiant
                $user->type='etudiant';
                $formation=Formation::findOrFail($request->formation_id);
                $formation->users()->save($user);

                $request->session()->flash('etat',"Modification de l'utilisateur administrateur en etudiant effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);
            }


        }elseif (($request->formation_id==0) && ($request->type=='admin')){//il veut changer l'utilisateur en administrateur
            if(($user->type=='admin') && ($user->formation_id==null)){
                $request->session()->flash('etat',"Aucune modification car l'utilisateur est deja un administrateur");
                return back();
            }

            if($user->type=='enseignant') {// modifier un administrateur en enseignant
                $cours_r=$user->coursUnique;

                foreach ($cours_r as $cour) {
                    //$form_cours = Cours::findOrFail($cour->id);

                    DB::delete('delete from cours_users where cours_id=?', [$cour->id]);

                    DB::delete('delete from plannings where cours_id=?', [$cour->id]);

                    $cour->delete();
                }
                $user->type='admin';
                $user->save();

                $request->session()->flash('etat',"Modification de l'utilisateur enseignant '$user->login' en administrateur effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);
            }

            if($user->type=='etudiant') { //modifier un etudiant en administrateur
                DB::delete('delete from cours_users where user_id=?',[$user->id]);
                //$user->cours->detach();
                $user->formation()->dissociate();
                $user->type='admin';
                $user->save();

                $request->session()->flash('etat',"Modification de l'utilisateur etudiant '$user->login' en administrateur effectuer");
                return redirect()->route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id]);

            }
        }

        $request->session()->flash('etat',"ATTENTION: l'enseignant et l'administrateur ne peuvent pas posseder de formation et un etudiant doit posseder une formation");
        return back();
    }

    //4.1.6. Suppression d’un utilisateur.

    /**
     * @param Request $request
     * @param $user_id l'id de l'utilisateurs à supprimer
     * @return \Illuminate\Http\RedirectResponse*
     */
    public function suppression_user(Request $request,$user_id){
        $user=User::findOrFail($user_id);

        if($user->type=='enseignant'){//suppression de tous les cours dont il est associer
            $cours_r=$user->coursUnique;

            foreach ($cours_r as $cour){
                $form_cours=Cours::findOrFail($cour->id);

                DB::delete('delete from cours_users where cours_id=?',[$form_cours->id]);

                DB::delete('delete from plannings where cours_id=?',[$form_cours->id]);

                $form_cours->delete();
            }
            $user->delete();
            $request->session()->flash('etat',"Suppression de l'enseignant effectuer");
            return back();

        }elseif ($user->type=='etudiant'){
            DB::delete('delete from cours_users where user_id=?',[$user->id]);
            //$user->cours->detach();
            $user->delete();
            $request->session()->flash('etat',"Suppression de l'etudiant effectuer");
            return back();
        }elseif ($user->type=='admin'){
            $user->delete();
            $request->session()->flash('etat',"Suppression de l'administrateur effectuer");
            return back();
        }
    }

    //4.1.1. Liste :

    public function liste_filtre_inte_etu_ensei($filtre){

        if($filtre=='integrale'){
            $users=User::where('type','<>',null)->get();
            return view('admin.users.liste.liste_filtre_inte_etu_ensei',['users'=>$users,'filtre'=>$filtre]);
        }elseif ($filtre=='etudiant'){
            $etudiants=User::where('type','=','etudiant')->get();
            return view('admin.users.liste.liste_filtre_inte_etu_ensei',['etudiants'=>$etudiants,'filtre'=>$filtre]);
        }elseif ($filtre=='enseignant'){
            $enseignants=User::where('type','=','enseignant')->get();
            return view('admin.users.liste.liste_filtre_inte_etu_ensei',['enseignants'=>$enseignants,'filtre'=>$filtre]);
        }elseif ($filtre=='admin'){
            $admins=User::where('type','=','admin')->get();
            return view('admin.users.liste.liste_filtre_inte_etu_ensei',['admins'=>$admins,'filtre'=>$filtre]);
        }
    }

}


