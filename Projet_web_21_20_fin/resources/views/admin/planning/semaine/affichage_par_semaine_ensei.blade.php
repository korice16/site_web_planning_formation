@extends('modele')

@section('title','affichage integrale')

@section('contents')
    <h1>Semaine du {{$st}} au {{$et}}</h1>
    <h3>
        <a href="{{route('admin.planning.semaine.affichage_par_semaine_v2_ensei',['type'=>$type,'user_id'=>$user_id,'page'=>'p','startweek'=>$st,'endweek'=>$et])}}">Semaine precedente</a>
        <a href="{{route('admin.planning.semaine.affichage_par_semaine_v2_ensei',['type'=>$type,'user_id'=>$user_id,'page'=>'s','startweek'=>$st,'endweek'=>$et])}}">Semaine suivante</a>
    </h3>
    @if($planning_e!=array())
        <table>
            <tr>
                <th>N°</th>
                <th>Nom</th>
                <th>Date debut</th>
                <th>Date fin</th>
            </tr>

            @foreach($planning_e as $planning)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours_e[$planning->cours_id]->intitule}}</td>
                    <td>{{$planning->date_debut}}</td>
                    <td>{{$planning->date_fin}}</td>
                </tr>
            @endforeach
        </table>
        <p><li><a href="{{route('admin.planning.cours.creation_seance_cours_form',['type'=>'enseignant_v2','user_id'=>$user_id])}}">Creation d'une seance de cours</a> </li></p>
    @else
        <p><h3>Aucune seance de cours n'est disponible cette semainee</h3></p>
    @endif

    @if($type=='admin')
        <p><h4><button><a href="{{route('admin.planning.semaine.home_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>

    @endif
@endsection
