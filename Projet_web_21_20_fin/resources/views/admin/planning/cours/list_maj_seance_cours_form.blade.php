@extends('modele')

@section('title','liste MAJ seance')

@section('contents')
    <h1>Liste possible des mises à jour d'une seance de cours</h1>

    @if(count($planning_r)!=0)
        <table>
            <tr>
                <th>N°</th>
                <th>Cours</th>
                <th>Date debut</th>
                <th>Date fin</th>
                <th>Action</th>
            </tr>
            @foreach($planning_r as $planning)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours[$planning->cours_id]->intitule}}</td>
                    <td>{{$planning->date_debut}}</td>
                    <td>{{$planning->date_fin}}</td>
                    <td><a href="{{route('admin.planning.cours.maj_seance_cours_form',['type'=>$type,'user_id'=>$user_id,'planning_id'=>$planning->id])}}">MAJ n°{{$loop->iteration}}</a>
                        / <a href="{{route('admin.planning.cours.supprimer_seance_cours',['type'=>$type,'user_id'=>$user_id,'planning_id'=>$planning->id])}}">Supp n°{{$loop->iteration}}</a> </td>
                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Impossible de mettre à jour une seance de cours car vous n'êtes responsable d'aucun seance de cours</h3></p>
    @endif

    <p><h4><button><a href="{{route('admin.planning.cours.home_cours_ensei',['type'=>$type,'user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
