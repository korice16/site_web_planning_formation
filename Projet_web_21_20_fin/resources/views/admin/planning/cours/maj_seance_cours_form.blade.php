@extends('modele')

@section('title',"creation d'une seance")

@section('contents')
    <h1>MAJ d'une seance de cours</h1>

    @if(!empty($plannings))
        <form method="post">
            <p>
                Cours : {{$cours_r->intitule}}
            </p>
            <h4>Modifier le cours (laisser les champs dont vous ne souhaittez pas modifier)</h4>
            <input type="hidden" name="planning_id" value="{{$plannings->id}}">

            <p>
                <label for="date_cours">Date du debut du cours</label>
                <input type="date" id="date_cours" name="date_cours" value="{{$date[0]->format('Y-m-d')}}">
            </p>

            <p>
                <label for="time_debut_nv">Heure du debut du cours</label>
                <input type="time" id="time_debut_nv" name="time_debut_nv" value="{{$date[0]->format('H:i')}}">
            </p>

            <p>
                <label for="time_fin_nv">Heure du fin du cours</label>
                <input type="time" id="time_fin_nv" name="time_fin_nv" value="{{$date[1]->format('H:i')}}">
            </p>

            <input type="submit" value="Envoyer modification">
            @csrf
        </form>

    @else
        <p><h3>Impossible de creer une seance de cours car vous n'êtes responsable d'aucun cours</h3></p>
    @endif

    @if($type=='admin')
        <p><h4><button><a href="{{route('admin.planning.cours.home_cours_ensei',['type' => $type, 'user_id' => $user_id])}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($type=='admin_v3')
        <p><h4><button><a href="{{route('admin.planning.semaine.affichage_par_semaine_act_v3_ensei',['type' => 'enseignant', 'user_id' => $user_id])}}"><--- Retour en arriere</a> </button></h4></p>

    @endif
@endsection
