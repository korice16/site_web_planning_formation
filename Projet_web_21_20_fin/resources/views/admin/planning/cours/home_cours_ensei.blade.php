@extends('modele')

@section('title','cours home')

@section('contents')
    <h1>Gestion du planning par cours</h1>

        <ol>
            <li><a href="{{route('admin.planning.cours.creation_seance_cours_form',['type'=>$type,'user_id'=>$user_id])}}">Creation d'une seance de cours</a> </li>
            <li><a href="{{route('admin.planning.cours.list_maj_seance_cours_form',['type'=>$type,'user_id'=>$user_id])}}">MAJ/Supression d'une seance de cours </a> </li>
        </ol>


    @if($type=='admin')
        <p><h4><button><a href="{{route('admin.planning.home_planning',['type'=>$type,'user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>

    @endif
@endsection
