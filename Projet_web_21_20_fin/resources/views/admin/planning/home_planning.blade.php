@extends('modele')

@section('title','home planning')

@section('contents')
    <h1>Gestion du planning par cours ou par semaine</h1>

        <ol>
            <li><a href="{{route('admin.planning.cours.home_cours_ensei',['type'=>$type,'user_id'=>$user_id])}}">Par Cours</a> </li>
            <li><a href="{{route('admin.planning.semaine.home_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id])}}">Par Semaine</a> </li>
        </ol>

    @if($type=='admin')
        <p><h4><button><a href="{{route('admin.home')}}"><--- Retour en arriere</a> </button></h4></p>
    @endif
@endsection
