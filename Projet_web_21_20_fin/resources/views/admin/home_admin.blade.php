@extends('modele')

@section('title','HOME admin')

@section('contents')
<h1>Admin House</h1>
    <ol >
        <li><a href="{{route('admin.users')}}">Gestion des utilisateurs</a> </li>
        <li><a href="{{route('admin.cours')}}">Gestion des cours</a> </li>
        <li><a href="{{route('admin.formations')}}">Gestion des formations</a> </li>
        <li><a href="{{route('admin.planning.list_enseignant',['definir'=>'planning'])}}">Gestion du planning</a> </li>

    </ol>

{{--}}
    <div class="topnav">
        <a href="{{route('admin.users')}}">Gestion des utilisateurs</a> <br>
        <a href="{{route('admin.cours')}}">Gestion des cours</a> <br>
        <a href="{{route('admin.formations')}}">Gestion des formations</a><br>
        <a href="{{route('admin.planning.list_enseignant',['definir'=>'planning'])}}">Gestion du planningV2</a><br>
        <a href="{{route('admin.planning.home_planning',['type'=>'admin','user_id'=>(Auth::User())->id])}}">Gestion du planning</a><br>
    </div>
--}}
<div class="sidenav">
    <a href="#">About</a>
    <a href="#">Services</a>
    <a href="#">Clients</a>
    <a href="#">Contact</a>
</div>
@endsection
