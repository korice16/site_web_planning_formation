@extends('modele')

@section('title','liste formation ')

@section('contents')
    <p><h1>Liste des formations</h1></p>

    <p><h3>Intitule</h3></p>
        <h4>
        <ul>
        @if(!empty($formations))
            @foreach($formations as $form)
                    <p><li> {{$loop->iteration}}. {{$form->intitule}}
                    <a href="{{route('admin.formations.modification',['formation_id'=>$form->id])}}">Modification</a>
                    / <a href="{{route('admin.formations.suppression',['formation_id'=>$form->id])}}">Supression</a>
                    </li></p>
            @endforeach
        @else
            <p><h3>Pas de formations</h3></p>
        @endif
        </ul>
        </h4>
    <p><h4><button><a href="{{route('admin.formations')}}"><--- Retour en arriere</a> </button></h4></p>
    {{$formations->links()}}
@endsection
