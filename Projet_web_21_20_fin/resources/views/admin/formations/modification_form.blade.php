@extends('modele')

@section('title','modification de formation')

@section('contents')
    <h1>Modification d'une formation</h1>
    <form method="post">
        <p><label for="intitule">Nom de la formation: </label>

            <input type="text" id="intitule" name="intitule" value="{{$formation->intitule}}"></p>
        <input type="submit" value="Envoyer">
        @csrf
    </form>
    <p><h4><button><a href="{{route('admin.formations')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
