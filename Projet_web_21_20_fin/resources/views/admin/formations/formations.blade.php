@extends('modele')

@section('title','admin/formations')

@section('contents')
<h1>Gestion des formations</h1>
    <ol>
        <li><a href="{{route('admin.formations.list_formations')}}">Liste des formations : Modification/Supression</a> </li>
        <li><a href="{{route('admin.formations.ajout_form')}}">Ajouter une formation</a> </li>
    </ol>
    <p><h4><button><a href="{{route('admin.home')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
