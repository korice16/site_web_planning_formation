@extends('modele')

@section('title','ajout de formation')

@section('contents')
    <h1>Ajout d'une formation</h1>
    <form action="{{route('admin.formations.ajout')}}" method="post">
        <p><label for="formation">Nom de la formation: </label>

        <input type="text" id="formation" name="formation" value="{{old('formation')}}"></p>
        <input type="submit" value="Envoyer">
        @csrf
    </form>
    <p><h4><button><a href="{{route('admin.formations')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
