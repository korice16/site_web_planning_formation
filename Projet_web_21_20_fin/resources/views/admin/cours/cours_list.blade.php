@extends('modele')

@section('title','list cours ')

@section('contents')
    <!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

<p><h1>Liste des cours</h1></p>
<p><h5><button><a href="{{route('admin.cours.creation_form')}}">Creer un cours</a> </button></h5></p>

<p><h5><button><a href="{{route('admin.cours')}}"><--- Retour en arriere</a> </button></h5></p>
    @if(!empty($cours))
        <input class="form-control" id="myInput" type="text" placeholder="Search.."> <br>
        <table>
            <tr>
                <th>N°</th>
                <th>Intituler</th>
                <th>Prof</th>
                <th>Action</th>

            </tr>
            @if(isset($cours))
                <tbody id="myTable">
                @foreach($cours as $cour)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$cour->intitule}}</td>
                        <td>{{$cours_nom[$cour->user_id]}}</td>
                        <td><a href="{{route('admin.cours.modification',['cours_id'=>$cour->id])}}">Modifier {{$loop->iteration}}</a>
                        / <a href="{{route('admin.cours.suppression',['cours_id'=>$cour->id])}}">Supression {{$loop->iteration}}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            @else
                <p><h3>Pas d'utilisateur en attente d'acceptation</h3></p>
            @endif
        </table>
    @else
        <p><h3>Aucun cours de disponible, veullez en creer</h3></p>
    @endif


<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

</body>
</html>
@endsection
