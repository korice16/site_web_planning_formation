@extends('modele')

@section('title','modification de cours')

@section('contents')
    <h1>Modification d'un cours</h1>
    <form method="post">
        <p><label for="intitule">Nom du cours: </label>
            <input type="text" id="intitule" name="intitule" value="{{$cours->intitule}}"></p>

        <p>
            Enseignant:
            <select name="user_id">
                <option value="{{$teacher_act->id}}">{{$teacher_act->login}}</option>
                @foreach($teachers as $teacher)
                    @if($teacher->id!=$teacher_act->id)
                        <option value="{{$teacher->id}}">{{$teacher->login}}</option>
                    @endif
                @endforeach
            </select>
        </p>

        <p>
            Formation:
            <select name="formation_id">
                <option value="{{$forma_act->id}}">{{$forma_act->intitule}}</option>
                @foreach($formation as $forma)
                    @if($forma->id!=$forma_act->id)
                        <option value="{{$forma->id}}">{{$forma->intitule}}</option>
                    @endif
                @endforeach
            </select>
        </p>
        <input type="submit" value="Modifier">
        @csrf
    </form>
    <p><h4><button><a href="{{route('admin.cours.cours_list')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
