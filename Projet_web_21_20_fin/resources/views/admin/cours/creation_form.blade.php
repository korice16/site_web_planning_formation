@extends('modele')

@section('title','Creation')

@section('contents')
    <p><h1>Creation d'un cours</h1></p>

    <h3>Introduire le nom du cours puis selectionenr le professeur dont le cours doit être assigner</h3>

    <form method="post">
        <p><h4><label for="intitule">Nom du cours</label>

        <input type="text" id="intitule" name="intitule" "></h4></p>

        @if(!empty($enseignant))
            @if(!empty($formation))
                <p><label for="formation">Choissisez la formation du cours</label>
                    <select name="formation_id">
                        @foreach($formation as $for)
                            <option value="{{$for->id}}">{{$for->intitule}}</option>
                        @endforeach
                    </select></p>

                <p><h3>Selectionenr l'enseignant</h3></p>

                <table>
                    <tr>
                        <th>selection</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Login</th>
                    </tr>

                    @foreach($enseignant as $ensei)
                        <tr>
                            <td><input type="radio" name="enseignant_id" value="{{$ensei->id}}"> {{$ensei->login}}</td>
                            <td>{{$ensei->nom}}</td>
                            <td>{{$ensei->prenom}}</td>
                            <td>{{$ensei->login}}</td>
                        </tr>
                    @endforeach
                </table>
                <br>

                <p><input type="submit" value="Envoyer"></p>
                @csrf
                </form>
            @else
                Pas de formation auxquelles donnes les cours
            @endif
        @else
            Pas d'enseignants
        @endif


    <p><h4><button><a href="{{route('admin.cours')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
