@extends('modele')

@section('title','admin/cours')

@section('contents')
<h1>Gestion des cours</h1>
    <ol>
        <li><a href="{{route('admin.cours.cours_list')}}">Liste des cours/ recherche / modification / Suppression</a> </li>
        <li><a href="{{route('admin.cours.creation_form')}}">Creer un cours</a> </li>
        <li><a href="{{route('admin.users.list_enseignant',['definir'=>'cours'])}}">Associer un enseignant a un cours / liste des cours d'un enseignant</a> </li>
    </ol>
    <p><h4><button><a href="{{route('admin.home')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
