@extends('modele')

@section('title','list cours')

@section('contents')

    @if(count($cours_r)!=0)
        <h1>Liste des cours de l'enseignant  "{{$teacher->login}}"</h1>
        <table>
            <tr>
                <th>N°</th>
                <th>Intituler</th>
                <th>Formation</th>
            </tr>

            @foreach($cours_r as $cours)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours->intitule}}</td>
                    <td>{{$formation[$cours->formation_id]->intitule}}</td>
                </tr>
            @endforeach

        </table>
    @else
        <p><h3>l'enseignant "{{$teacher->login}}" responsable d'aucun cours</h3></p>
    @endif

@endsection
