@extends('modele')

@section('title','Liste utilisateurs')

@section('contents')
    <!DOCTYPE>
    <html>
        <head>
            <meta charset="utf-8">

            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
            <style>
                /* Style the input field */
                #myInput {
                    padding: 20px;
                    margin-top: -6px;
                    border: 0;
                    border-radius: 0;
                    background: #f1f1f1;
                }
            </style>
        </head>
        <body>

        <br><p>
            <input class="form-control" id="myInput" type="text" placeholder="Search.."> </p>

        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{$filtre}}
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
                <li><a href="{{route('admin.users.liste.liste_filtre_inte_etu_ensei',['filtre'=>'integrale'])}}">Integrale</a></li>
                <li><a href="{{route('admin.users.liste.liste_filtre_inte_etu_ensei',['filtre'=>'admin'])}}">Administrateur</a></li>
                <li><a href="{{route('admin.users.liste.liste_filtre_inte_etu_ensei',['filtre'=>'etudiant'])}}">Etudiant</a></li>
                <li><a href="{{route('admin.users.liste.liste_filtre_inte_etu_ensei',['filtre'=>'enseignant'])}}">Enseignant</a></li>
            </ul>
        </div>


        @if($filtre=='integrale')
            <h1>Filtrage integrale des utilisateurs</h1>
        @elseif($filtre=='etudiant')
            <h1>Filtrage des utilisateurs par etudiants</h1>
        @elseif($filtre=='enseignant')
            <h1>Filtrage des utilisateurs par enseignant</h1>
        @endif
        <p><h4><button><a href="{{route('admin.users')}}"><--- Retour en arriere</a> </button></h4></p>
        @if($filtre=='integrale')
            @if( count($users)!=0)
                <table id="myTable">
                    <tr>
                        <th>N°</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Login</th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$user->nom}}</td>
                            <td>{{$user->prenom}}</td>
                            <td>{{$user->login}}</td>


                        </tr>
                    @endforeach
                </table>
            @else
                <p><h3>Aucun utilisateur n'est defini</h3></p>
            @endif

        @elseif($filtre=='etudiant')
            @if( count($etudiants)!=0)
                <table id="myTable">
                    <tr>
                        <th>N°</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Login</th>
                    </tr>
                    @foreach($etudiants as $etu)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$etu->nom}}</td>
                            <td>{{$etu->prenom}}</td>
                            <td>{{$etu->login}}</td>


                        </tr>
                    @endforeach
                </table>
            @else
                <p><h3>Aucun etudiant n'est disponible</h3></p>
            @endif

        @elseif($filtre=='enseignant')
            @if( count($enseignants)!=0)
                <table id="myTable">
                    <tr>
                        <th>N°</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Login</th>
                    </tr>
                    @foreach($enseignants as $ensei)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$ensei->nom}}</td>
                            <td>{{$ensei->prenom}}</td>
                            <td>{{$ensei->login}}</td>


                        </tr>
                    @endforeach
                </table>
            @else
                <p><h3>Aucun enseignant n'est disponible</h3></p>
            @endif

        @elseif($filtre=='admin')
            @if( count($admins)!=0)
                <table id="myTable">
                    <tr>
                        <th>N°</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Login</th>
                    </tr>
                    @foreach($admins as $admin)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$admin->nom}}</td>
                            <td>{{$admin->prenom}}</td>
                            <td>{{$admin->login}}</td>


                        </tr>
                    @endforeach
                </table>
            @else
                <p><h3>Aucun enseignant n'est disponible</h3></p>
            @endif

        @endif


        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                    $(".dropdown-menu li").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>
        </body>
    </html>
@endsection
