@extends('modele')

@section('title','acceptation/refus ')

@section('contents')
    <p><h1>Acception ou refus d'un utilisateur</h1></p>
    @if(count($user)!=0)

        <table>
            <tr>
                <th>N°</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>login</th>
                <th>Formation</th>
                <th>Type apres acceptation</th>
                <th>Accepter</th>
                <th>Refuser</th>
            </tr>

            @foreach($user as $use)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$use->nom}}</td>
                    <td>{{$use->prenom}}</td>
                    <td>{{$use->login}}</td>
                    @if(isset($formation[$use->id]))
                        <td>{{$formation[$use->id]}}</td>
                    @else
                        <td>Pas de formation</td>
                    @endif
                    @if($use->formation_id==null)
                        <td>Enseignant</td>
                    @else
                        <td>Etudiant</td>
                    @endif
                    <td><button ><a href="{{route('admin.users.acceptation',['user_id'=>$use->id])}}">Accepter {{$loop->iteration}}</a> </button> </td>
                    <td><button ><a href="{{route('admin.users.refus',['user_id'=>$use->id])}}">Refuser {{$loop->iteration}}</a> </button> </td>
                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Pas d'utilisateur en attente d'acceptation ou de refus</h3></p>

    @endif
    <p><h4><button><a href="{{route('admin.users')}}"><--- Retour en arriere</a> </button></h4></p>
    {{$user->links()}}
@endsection
