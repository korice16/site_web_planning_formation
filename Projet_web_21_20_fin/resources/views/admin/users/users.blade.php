@extends('modele')

@section('title','admin/users')

@section('contents')
<h1>Gestion des utilisateurs</h1>
    <ol>
        <li><a href="{{route('admin.users.liste.liste_filtre_inte_etu_ensei',['filtre'=>'integrale'])}}">Liste des utilisateurs</a> </li>
        <li><a href="{{route('admin.users.acceptation_refus_form')}}">Acceptaion/refus d'un utilisateur</a> </li>
        <li><a href="{{route('admin.users.list_enseignant',['definir'=>'users'])}}">Associer un enseignant a un cours</a> </li>
        <li><a href="{{route('admin.users.creation_utilisateur')}}">Creation d'un utilisateur</a> </li>
        <li><a href="{{route('admin.users.modifier_user.list_users_integrale')}}">Modification / Supression d'un utilisateur</a> </li>
        <li><a href="{{route('admin.users.modifier_user.list_users_integrale')}}">Supression d'un utilisateur</a> </li>
    </ol>
    <p><h4><button><a href="{{route('admin.home')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
