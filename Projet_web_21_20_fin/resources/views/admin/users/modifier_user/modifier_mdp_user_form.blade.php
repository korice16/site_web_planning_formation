@extends('modele')

@section('title','Modifier mdp')

@section('contents')
    <h1>Modifier mdp</h1>
    <h3>Saissisez les informations necessaires pour modifier le mot de passe de "{{$user->login}}"</h3>
    <form method="post">

        <p></p><label for="fmdp">Nouveau Password :</label>
        <input type="password" id="fmdp" name="mdp"></p>

        <p></p><label for="fcmdp">Confirmer nouveau Password :</label>
        <input type="password" id="fcmdp" name="mdp_confirmation"></p>

        <input type="submit" value="Modifier mdp">
        @csrf
    </form>
    <p><h4><button><a href="{{route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id])}}"><--- Retour en arriere</a></button></h4></p>
@endsection
