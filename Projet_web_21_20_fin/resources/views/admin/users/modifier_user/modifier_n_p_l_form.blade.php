@extends('modele')

@section('title','Modifier nom/prenom')

@section('contents')
    <h1>Modifier nom/prenom/login </h1>
    <h3>Saissisez les informations necessaires pour modifier le nom/prenom/login de "{{$user->login}}"</h3>
    <form method="post">

        <p><label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom" value="{{$user->nom}}"></p>

        <p><label for="prenom">Prenom :</label>
        <input type="text" id="prenom" name="prenom" value="{{$user->prenom}}"></p>

        <p><label for="login">Login :</label>
        <input type="text" id="login" name="login" value="{{$user->login}}"></p>

        <input type="submit" value="Modifier">
        @csrf
    </form>
    <p><h4><button><a href="{{route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user_id])}}"><--- Retour en arriere</a></button></h4></p>
@endsection
