@extends('modele')

@section('title',"modification d'un utilisateur")

@section('contents')
<h1>Modification de l'utilisateur "{{$user->login}}"</h1>
    <ol>
        <li><a href="{{route('admin.users.modifier_user.modifier_n_p_l_form',['user_id'=>$user_id])}}">Modifier nom/prenom/login</a> </li>
        <li><a href="{{route('admin.users.modifier_user.modifier_mdp_user_form',['user_id'=>$user_id])}}">Modifier mot de passe</a> </li>
        <li><a href="{{route('admin.users.modifier_user.modifier_formation_type_form',['user_id'=>$user_id])}}">Modifier formation/type</a> </li>
    </ol>
    <p><h4><button><a href="{{route('admin.users.modifier_user.list_users_integrale')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
