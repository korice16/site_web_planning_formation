@extends('modele')

@section('title','list utilisateur')

@section('contents')
    <h1>Modification / Supression d'un utilisateur </h1>
    <p><h4><button><a href="{{route('admin.users')}}"><--- Retour en arriere</a> </button></h4></p>
    @if(count($users)!=0)
        <table>
            <tr>
                <th>N°</th>
                <th>Login</th>
                <th>Action</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$user->login}}</td>
                    <td><a href="{{route('admin.users.modifier_user.modifier_user_home',['user_id'=>$user->id])}}">Modifier </a>
                     / <a href="{{route('admin.users.suppression',['user_id'=>$user->id])}}">Supprimer </a></td>

                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Aucun utilisateur n'est defini</h3></p>
    @endif

    {{$users->links()}}
@endsection
