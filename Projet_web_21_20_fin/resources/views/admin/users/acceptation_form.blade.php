@extends('modele')

@section('title','acceptation ')

@section('contents')
    <p><h1>Acceptation</h1></p>
    <p>Nom: {{$user->nom}}</p>
    <p>Prenom: {{$user->prenom}}</p>
    <p>Login: {{$user->login}}</p>
    @if(!empty($formation))
        <p>Formation: {{$formation}}</p>
    @else
        <p>Formation: pas de formation ( car enseignant)</p>
    @endif
    <form method="post">
        <p>Choississez le type de cette utilisateur
            <select name="type">
                <option value="etudiant">Etudiant</option>
                <option value="enseignant">Enseignant</option>
            </select></p>
        <input type="submit" value="Envoyer">
        @csrf
    </form>
    <p><h4><button><a href="{{route('admin.users.acceptation_refus_form')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
