@extends('modele')

@section('title','acceptation/refus ')

@section('contents')
    @if($definir=='users')
        <p><h1>Associer un enseignant a un cours</h1></p>
    @elseif($definir=='planning')
        <p><h1>Gestion du planning</h1></p>
    @elseif($definir=='cours')
        <p><h1>Associer un enseignant a un cours / Liste des cours de l'enseignant</h1></p>
    @endif

    <table>
        <tr>
            <th>N°</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>login</th>

            <th>Action</th>

        </tr>
        @if(isset($enseignants))
            @foreach($enseignants as $teacher)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$teacher->nom}}</td>
                    <td>{{$teacher->prenom}}</td>
                    <td>{{$teacher->login}}</td>
                    @if($definir=='planning')
                        <td><a href="{{route('admin.planning.home_planning',['type'=>'admin','user_id'=>$teacher->id])}}">Gestion du planning, N°{{$loop->iteration}}</a></td>>
                    @elseif($definir=='cours')
                        <td><button ><a href="{{route('admin.users.associer_enseignant_cours_form',['teacher_id'=>$teacher->id,'definir'=>$definir])}}">Associer a un cours </a> </button>
                            <button><a href="{{route('admin.cours.list_cours_enseignant',['user_id'=>$teacher->id])}}">Liste des cours</a> </button>
                        </td>
                    @elseif($definir=='users')
                        <td><button ><a href="{{route('admin.users.associer_enseignant_cours_form',['teacher_id'=>$teacher->id,'definir'=>$definir])}}">Associer a un cours </a> </button></td>
                    @endif
                </tr>
            @endforeach
        @else
            <p><h3>Pas d'utilisateur en attente d'acceptation</h3></p>
        @endif
    </table>
    @if($definir=='users')
        <p><h4><button><a href="{{route('admin.users')}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($definir=='planning')
        <p><h4><button><a href="{{route('admin.home')}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($definir=='cours')
        <p><h4><button><a href="{{route('admin.cours')}}"><--- Retour en arriere</a> </button></h4></p>
    @endif
    {{$enseignants->links()}}
@endsection
