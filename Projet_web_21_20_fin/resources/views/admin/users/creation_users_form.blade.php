
@extends('modele')

@section('title','Enregistrement')

@section('contents')
    <h1>Enregistrement</h1>
    @if(count($formation)!=0)
        <form method="post">
            <p><label for="fnom">Nom :</label>
                <input type="text" id="fnom" name="nom" ></p>

            <p><label for="fprenom">Prenom :</label>
                <input type="texte" id="fprenom" name="prenom" ></p>

            <p><label for="flogin">Login :</label>
                <input type="text" id="flogin" name="login" ></p>

            <p><label for="formation">Choissisez la formation</label>
                <select name="formation_id">
                    <option value="-1">Administrateur</option>
                    <option value="0">Enseignant</option>
                    @foreach($formation as $for)
                        <option value="{{$for->id}}">{{$for->intitule}}</option>
                    @endforeach
                </select></p>
            <p><label for="fmdp">Password :</label>
                <input type="password" id="fmdp" name="mdp"></p>

            <p><label for="fcmdp">Confirmation Password : </label>
                <input type="password" id="fcmdp" name="mdp_confirmation"></p>

            <p><input type="submit" value="Creer compte"></p>
            @csrf
        </form>
    @else
        Pas de formation disponible dans l'etablissement, impossible de s'inscrire, veuillez reesayer apres l'ajout d'une formation
    @endif

    <p><h4><button><a href="{{route('admin.users')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
