@extends('modele')

@section('title','associer cours a un prof ')

@section('contents')
    <p><h1>Associer l'enseignant a un cours</h1></p>
    <p><h2>Le prof dont le cours doit etre assigner</h2></p>
    <p>Nom: {{$teacher->nom}}</p>
    <p>Prenom: {{$teacher->prenom}}</p>
    <p>Login: {{$teacher->login}}</p>

    @if(!empty($cours))
        <p><h3>Si vous associez l'enseigant a un cours d'un autre enseignant , le prof actuelle du cours sera remplacer </h3></p>
        <table>
            <tr>
                <th>N°</th>
                <th>Intituler</th>
                <th>Prof actuelle[login]</th>
                <th>Modification</th>

            </tr>
            @if(isset($cours))
                @foreach($cours as $cour)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$cour->intitule}}</td>
                        <td>{{$cours_nom[$cour->user_id]}}</td>
                        <td><a href="{{route('admin.users.associer_enseignant_cours',['teacher_id'=>$teacher->id,'cours_id'=>$cour->id,'definir'=>$definir])}}">associer à ce cours {{$loop->iteration}}</a> </td>
                    </tr>
                @endforeach
            @else
                <p><h3>Pas d'utilisateur en attente d'acceptation</h3></p>
            @endif
        </table>
    @else
        <p><h3>Pas de cours disponible pour etre attribuer à un enseignant, veuillez en creer</h3></p>
    @endif
    <p><h4><button><a href="{{back()->getTargetUrl()}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
