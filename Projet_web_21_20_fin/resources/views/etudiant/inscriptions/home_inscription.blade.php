@extends('modele')

@section('title','inscription')

@section('contents')
    <ul>
        <p><li><a href="{{route('etudiant.gestion_inscription.inscription')}}">Inscription</a> </li></p>
        <p><li><a href="{{route('etudiant.gestion_inscription.list_cours',['type'=>'gestion_inscription'])}}">Liste des cours auxquels on est inscrit/desinscription d'un cours</a> </li></p>
        <p><li><a href="{{route('etudiant.list_cours',['but'=>'recherche'])}}">Rechercher un cours</a> </li></p>
    </ul>
    <p><h4><button><a href="{{route('etudiant.home_etudiant')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
