@extends('modele')

@section('title','inscription')

@section('contents')
    <h1>Inscription a un cours</h1>


    @if(!empty($cours))
        <form method="post">
            <p>Choississez le cours au quel vous souhaitez vous inscrire
                <select name="cours_id">
                    @foreach($cours as $cour)
                        <option value={{$cour->id}}>{{$cour->intitule}}</option>
                    @endforeach
                </select></p>
            <input type="submit" value="Inscription">
            @csrf
        </form>
    @else
        <p>Aucun cours n'est disponible pour une inscription</p>
    @endif

    <p><h4><button><a href="{{route('etudiant.home_inscription')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
