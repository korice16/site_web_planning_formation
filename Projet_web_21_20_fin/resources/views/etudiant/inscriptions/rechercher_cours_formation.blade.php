@extends('etudiant.inscriptions.modele_search_li')

@section('title','liste cours')

@section('contenu')
    <h1>Liste des cours de la formation "{{$forma_nom}}"</h1>
    <input id="myInput" type="text" placeholder="Search..">


    @if(count($cours)!=0)
        <ul id="myList">
            @foreach($cours as $cour)
                <li>{{$loop->iteration}} {{$cour->intitule}}</li>
            @endforeach
        </ul>
    @else
        <p><h3>Vous n'avez pas de cours diponible dasn votre formation</h3></p>
    @endif

    <p><h4><button><a href="{{back()->getTargetUrl()}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
