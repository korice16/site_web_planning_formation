@extends('modele')

@section('title','cours')

@section('contents')
    <h1>Liste des cours de la formation "{{$forma_nom}}"</h1>

    @if(count($cours)!=0)
        <ul>
        @foreach($cours as $cour)
            <li>{{$loop->iteration}} {{$cour->intitule}}</li>
        @endforeach
        </ul>
    @else
        <p><h3>Vous n'avez pas de cours diponible dasn votre formation</h3></p>
    @endif

    <p><h4><button><a href="{{back()->getTargetUrl()}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
