@extends('modele')

@section('title','HOME etudiant')

@section('contents')
    <p>
        <ul>
            <p><li><a href="{{route('etudiant.list_cours',['but'=>'list'])}}">Liste des cours de la formation</a> </li></p>
            <p><li><a href="{{route('etudiant.home_inscription')}}">Gestion des inscriptions</a> </li></p>
            <p><li><a href="{{route('etudiant.planning_perso.home_planning_perso')}}">Affichage planning</a> </li></p>
        </ul>
    </p>
@endsection
