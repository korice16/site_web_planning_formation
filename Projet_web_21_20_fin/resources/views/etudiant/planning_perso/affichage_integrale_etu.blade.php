@extends('modele')

@section('title','affichage integrale')

@section('contents')
    <h1>Affichage integrale des seances de vos cours</h1>

    @if($planning_e!=array())
        <table>
            <tr>
                <th>N°</th>
                <th>Nom</th>
                <th>Date debut</th>
                <th>Date fin</th>
            </tr>

            @foreach($planning_e as $planning)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours_e[$planning->cours_id]->intitule}}</td>
                    <td>{{$planning->date_debut}}</td>
                    <td>{{$planning->date_fin}}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Aucune seance de vos cours n'est disponible</h3></p>
    @endif
    <p><h4><button><a href="{{route('etudiant.planning_perso.home_planning_perso')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
