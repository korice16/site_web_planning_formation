@extends('modele')

@section('title','home planning')

@section('contents')

    <ul>
        <p><li><a href="{{route('etudiant.planning_perso.affichage_integrale_etu')}}">affichage integrale du planning</a> </li></p>
        <p><li><a href="{{route('etudiant.planning_perso.list_cours',['type'=>'planning_perso_etu'])}}">Affichage du planning par cours</a> </li></p>
        <p><li><a href="{{route('etudiant.planning_perso.affichage_par_semaine_act_etu')}}">affichage du planning par semaine</a> </li></p>
    </ul>

    <p><h4><button><a href="{{route('etudiant.home_etudiant')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
