@extends('modele')

@section('title','liste cours')

@section('contents')
    <h1>Liste des cours auxquels vous etes inscrit</h1>

    @if(count($cours_deja)!=0)
        <table>
            <tr>
                <th>N°</th>
                <th>Matiere</th>
                <th>Action</th>
            </tr>

            @foreach($cours_deja as $cour)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours_tab[$cour->cours_id]->intitule}}</td>
                    <td><a href="{{route('etudiant.planning_perso.affichage_par_cours_etu',['cours_id'=>$cour->cours_id])}}">Affichage du planning du cours de "{{$cours_tab[$cour->cours_id]->intitule}}"</a> </td>
                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Vous n'êtes inscrit à aucun cours</h3></p>
    @endif
    <p><h4><button><a href="{{route('etudiant.planning_perso.home_planning_perso')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
