<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>


        table{
            font-size: 30px;
            width: 100%;

        }

        h1{
            text-align: center;
        }

        table,th,td {
            border: 2px solid black;
            border-collapse: collapse;
            padding: 20px;
            text-align:center;
        }

        .etat{
            background-color: lightblue;
        }
        .error{
            background-color: lightpink;
        }
        .center {
            margin-left: auto;
            margin-right: auto;
        }
        #t01{
            width: 100%;
            background-color: #f1f1c1;
        }

        * {box-sizing: border-box;}
        body {
            background-color: lightblue;
            font-size: 10px;
            margin: 0;
            font-family: Arial, Helvetica,  "Lato", sans-serif;
        }

        .topnav {
            overflow: hidden;
            background-color: #e9e9e9;
        }

        .topnav a {
            float: left;
            display: block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav a:hover {
            background-color: #ddd;
            color: black;
        }

        .topnav a.active {
            background-color: #2196F3;
            color: white;
        }

        .topnav .login-container {
            float: right;
        }

        .topnav input[type=text] {
            padding: 6px;
            margin-top: 8px;
            font-size: 17px;
            border: none;
            width:120px;
        }

        .topnav .login-container button {
            float: right;
            padding: 6px 10px;
            margin-top: 8px;
            margin-right: 16px;
            background-color: #555;
            color: white;
            font-size: 17px;
            border: none;
            cursor: pointer;
        }

        .topnav .login-container button:hover {
            background-color: green;
        }

        @media screen and (max-width: 600px) {
            .topnav .login-container {
                float: none;
            }
            .topnav a, .topnav input[type=text], .topnav .login-container button {
                float: none;
                display: block;
                text-align: left;
                width: 100%;
                margin: 0;
                padding: 14px;
            }
            .topnav input[type=text] {
                border: 1px solid #ccc;
            }
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

        .sidenav a {
            padding: 8px 8px 8px 32px;
            text-decoration: none;
            font-size: 25px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }

        .sidenav a:hover {
            color: #f1f1f1;
        }

        .sidenav .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }

        @media screen and (max-height: 450px) {
            .sidenav {padding-top: 15px;}
            .sidenav a {font-size: 18px;}
        }
        a:link {
            color: red;
        }

        /* visited link */
        a:visited {
            color: cadetblue;
        }

        /* mouse over link */
        a:hover {
            color: hotpink;
        }

        /* selected link */
        a:active {
            color: blue;
        }

        ul,ol {
            display: block;

            line-height:250%;
            font-size: 25px;
            margin-top: 1em;
            margin-bottom: 1em;
            margin-left: 0;
            margin-right: 0;
            padding-left: 40px;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body >
@section('menu')
    @guest()
        <div class="topnav">
            <a class="active" href="/">Home</a>
            <a href="{{route('register')}}">Enregistrement</a>
            <div class="login-container">
                <form action="/modele/login" method="post">
                    <input type="text" placeholder="login" name="login">
                    <input type="password" placeholder="Password" name="mdp">
                    <button type="submit">Login</button>
                    @csrf
                </form>
            </div>
        </div>
    @endguest
    {{--}}
    <table>
        @guest()
            <th><a href="{{route('login')}}">Login</a></th>
            <th><a href="{{route('register')}}">Enregistrement</a></th>

        @endguest
    </table>
--}}

    @auth
        {{--}}
        <div class="topnav">
            @if(Auth::user()->type=='admin')
                <a class="active" href="{{route('admin.home')}}">Home: {{Auth::user()->login}} (admin)</a>
            @elseif(Auth::user()->type=='etudiant')
                <a class="active" href="{{route('etudiant.home_etudiant')}}">Home:{{Auth::user()->login}} (Etudiant)</a>
            @else
                <a class="active" href="{{route('enseignant.home_enseignant')}}">Home: {{Auth::user()->login}} (Enseignant)</a>
            @endif
            <th><a href="{{route('compte.profile')}}">Profile</a> </th>
            <a class="login-container" href="{{route('logout')}}">Deconnexion</a>
        </div>
        --}}
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            @if(Auth::user()->type=='admin')
                <a class="active" href="{{route('admin.home')}}">Home: {{Auth::user()->login}} (admin)</a>
            @elseif(Auth::user()->type=='etudiant')
                <a class="active" href="{{route('etudiant.home_etudiant')}}">Home:{{Auth::user()->login}} (Etudiant)</a>
            @else
                <a class="active" href="{{route('enseignant.home_enseignant')}}">Home: {{Auth::user()->login}} (Enseignant)</a>
            @endif
            <th><a href="{{route('compte.profile')}}">Profile</a> </th>
            <a class="login-container" href="{{route('logout')}}">Deconnexion</a>
        </div>

        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>

        <script>
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
        </script>
    @endauth

    {{--}}
    <table>
        @auth
            <tr>
                <th>{{Auth::user()->login}}: {{Auth::user()->type}}</th>
                <th><a href="{{route('logout')}}">Deconnexion</a></th>
                @if(Auth::user()->type=='admin')
                    <th><a href="{{route('admin.home')}}">Partie admin</a> </th>
                @elseif(Auth::user()->type=='etudiant')
                    <th><a href="{{route('etudiant.home_etudiant')}}">Etudiant</a> </th>
                @else
                    <th><a href="{{route('enseignant.home_enseignant')}}">Enseignant</a> </th>
                @endif
                <th><a href="{{route('compte.profile')}}">Profile</a> </th>
            </tr>
        @endauth
    </table>
--}}

@show

@section('etat')
    @if(session()->has('etat'))
        <p class="etat">{{session()->get('etat')}}</p>
    @endif
@show

@section('errors')
    @if ($errors->any())
        <div class="error">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@show




    @yield('contents')


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>

</html>
