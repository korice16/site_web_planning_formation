@extends('modele')

@section('title','profile')

@section('contents')
    <h1>Profil</h1>
    <ul>
        <p><li><a href="{{route('compte.profile.modifier_mdp_form')}}">Changer son mot de passe </a> </li></p>
        <p><li><a href="{{route('compte.profile.modifier_nom_prenom_form')}}">Changer son nom et/ou prenom </a> </li></p>

    </ul>
@endsection
