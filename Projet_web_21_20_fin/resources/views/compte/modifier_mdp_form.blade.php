@extends('modele')

@section('title','Modifier mdp')

@section('contents')
    <h1>Modifier mdp</h1>
    <h3>Saissisez les informations necessaires pour modifier votre mot de passe</h3>
    <form method="post">

        <p></p><label for="fa_mdp">Ancien Password :</label>
        <input type="password" id="fa_mdp" name="a_mdp"></p>

        <p></p><label for="fmdp">Nouveau Password :</label>
        <input type="password" id="fmdp" name="mdp"></p>

        <p></p><label for="fcmdp">Confirmer nouveau Password :</label>
        <input type="password" id="fcmdp" name="mdp_confirmation"></p>

        <input type="submit" value="Envoyer">
        @csrf
    </form>
    <a href="{{redirect()->back()->getTargetUrl()}}">Retour en arriere</a>
@endsection
