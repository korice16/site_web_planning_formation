@extends('modele')

@section('title','Modifier nom/prenom')

@section('contents')
    <h1>Modifier nom/prenom</h1>
    <h3>Saissisez les informations necessaires pour modifier votre nom et/ou prenom</h3>
    <form method="post">

        <p></p><label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom" value="{{$user->nom}}"></p>

        <p></p><label for="prenom">Prenom :</label>
        <input type="text" id="prenom" name="prenom" value="{{$user->prenom}}"></p>

        <input type="submit" value="Modifier">
        @csrf
    </form>
    <p><h4><button><a href="{{redirect()->back()->getTargetUrl()}}"><--- Retour en arriere</a></button></h4></p>
@endsection
