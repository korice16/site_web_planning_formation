
@extends('modele')

@section('title','Enregistrement')

@section('contents')
    <p>Enregistrement</p>
    @if(count($formation)==0)
        Pas de formation disponible dans l'etablissement, impossible de s'inscrire pour les etudiant, veuillez reesayer apres l'ajout d'une formation, uniquement les ensignants peuvent s'inscrire
    @endif
        <form method="post">
            <p><label for="fnom">Nom :</label>
                <input type="text" id="fnom" name="nom" value="{{old('nom')}}"></p>

            <p><label for="fprenom">Prenom :</label>
                <input type="texte" id="fprenom" name="prenom" value="{{old('prenom')}}"></p>

            <p><label for="flogin">Login :</label>
                <input type="text" id="flogin" name="login" value="{{old('login')}}"></p>

                <p><label for="formation">Choissisez votre formation</label>
                    <select name="formation_id">
                        <option value="0">Enseignant</option>
                        @foreach($formation as $for)
                            <option value="{{$for->id}}">{{$for->intitule}} (etudiant)</option>
                        @endforeach
                    </select></p>
            <p><label for="fmdp">Password :</label>
                <input type="password" id="fmdp" name="mdp"></p>

            <p><label for="fcmdp">Confirmation Password : </label>
                <input type="password" id="fcmdp" name="mdp_confirmation"></p>

            <p><input type="submit" value="Envoyer"></p>
            @csrf
        </form>

@endsection
