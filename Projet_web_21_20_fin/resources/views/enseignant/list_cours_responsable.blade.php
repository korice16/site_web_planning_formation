@extends('modele')

@section('title','list cours')

@section('contents')
    <h1>Liste des cours dont vous êtes responsable </h1>

    @if(count($cours_r)!=0)
        <ul>
            @foreach($cours_r as $cours)
                <li>{{$loop->iteration}} {{$cours->intitule}}</li>
            @endforeach
        </ul>
    @else
        <p><h3>Vous êtes responsable d'aucun cours</h3></p>
    @endif
@endsection
