@extends('modele')

@section('title','affichage integrale')

@section('contents')
    <h1>Semaine du {{$st}} au {{$et}}</h1>
    <h3>
        <a href="{{route('enseignant.planning_perso.affichage_par_semaine_ensei',['user_id'=>(Auth::User())->id,'type'=>'p','startweek'=>$st,'endweek'=>$et])}}">Semaine precedente</a>
        <a href="{{route('enseignant.planning_perso.affichage_par_semaine_ensei',['user_id'=>(Auth::User())->id,'type'=>'s','startweek'=>$st,'endweek'=>$et])}}">Semaine suivante</a>
    </h3>
    @if($planning_e!=array())
        <table>
            <tr>
                <th>N°</th>
                <th>Nom</th>
                <th>Date debut</th>
                <th>Date fin</th>
            </tr>

            @foreach($planning_e as $planning)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours_e[$planning->cours_id]->intitule}}</td>
                    <td>{{$planning->date_debut}}</td>
                    <td>{{$planning->date_fin}}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Aucune seance de cours n'est disponible cette semainee</h3></p>
    @endif
    <p><h4><button><a href="{{route('enseignant.planning_perso.home_planning_perso')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
