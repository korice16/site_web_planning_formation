@extends('modele')

@section('title','cours planning')

@section('contents')
    <h1>Planning du cours de "{{$cours->intitule}}"</h1>

    @if(count($planning)!=0)
        <table>
            <tr>
                <th>N°</th>
                <th>Date debut</th>
                <th>Date fin</th>
            </tr>

            @foreach($planning as $plan)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$plan->date_debut}}</td>
                    <td>{{$plan->date_fin}}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p><h3>Le cours {{$cours->intitule}} n'a pas de seance</h3></p>
    @endif
    <p><h4><button><a href="{{route('enseignant.planning_perso.list_cours_responsable',['type'=>'planning_perso_ensei'])}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
