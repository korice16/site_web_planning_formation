@extends('modele')

@section('title','list cours')

@section('contents')
    <h1>Liste des cours dont vous êtes responsable </h1>

    @if(count($cours_r)!=0)
        <ul>
            @foreach($cours_r as $cours)
                <li>{{$loop->iteration}} {{$cours->intitule}}  :  <a href="{{route('enseignant.planning_perso.affichage_par_cours_ensei',['cours_id'=>$cours->id])}}">Afficher le planning du cours "{{$cours->intitule}}"</a> </li>
            @endforeach
        </ul>
    @else
        <p><h3>Vous êtes responsable d'aucun cours</h3></p>
    @endif

    <p><h4><button><a href="{{route('enseignant.planning_perso.home_planning_perso')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
