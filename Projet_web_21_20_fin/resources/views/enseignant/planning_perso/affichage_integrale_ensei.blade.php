@extends('modele')

@section('title','affichage integrale cours')

@section('contents')
    <h1>Affichage integrale des seances de vos cours</h1>

    @if(count($planning_r)!=array())
        <table>
            <tr>
                <th>N°</th>
                <th>Cours</th>
                <th>Date debut</th>
                <th>Date fin</th>
            </tr>
            @foreach($planning_r as $planning)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours[$planning->cours_id]->intitule}}</td>
                    <td>{{$planning->date_debut}}</td>
                    <td>{{$planning->date_fin}}</td>
            @endforeach
        </table>
    @else
        <p><h3>Impossible d'afficher des seances de cours car vous n'êtes responsable d'aucun seance de cours</h3></p>
    @endif

    <p><h4><button><a href="{{route('enseignant.planning_perso.home_planning_perso')}}"><--- Retour en arriere</a> </button></h4></p>
@endsection
