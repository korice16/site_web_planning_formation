@extends('modele')

@section('title','home enseignant')

@section('contents')

    <ul>
        <p><li><a href="{{route('enseignant.cours.list_cours_responsable',['type'=>'list_cours_responsable'])}}">Liste de vos cours</a> </li></p>
        <p><li><a href="{{route('enseignant.planning_perso.home_planning_perso')}}">Affichage du planning</a> </li></p>
        <p><li><a href="{{route('enseignant.planning.home_planning',['type'=>'enseignant','user_id'=>(Auth::User())->id])}}">Gestion du planning</a> </li></p>

    </ul>

@endsection
