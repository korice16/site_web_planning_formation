@extends('modele')

@section('title','home planning')

@section('contents')
    <h1>Gestion du planning par cours ou par semaine</h1>
    <p>
        <ul>
            <p><li><a href="{{route('enseignant.planning.cours.home_cours_ensei',['type'=>$type,'user_id'=>$user_id])}}">Par Cours</a> </li></p>
            <p><li><a href="{{route('enseignant.planning.semaine.home_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id])}}">Par Semaine</a> </li></p>
        </ul>
    </p>
    @if($type=='enseignant')
        <p><h4><button><a href="{{route('enseignant.home_enseignant')}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($type=='admin')

    @endif
@endsection
