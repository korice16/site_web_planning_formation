@extends('modele')

@section('title',"creation d'une seance")

@section('contents')
    <h1>Creation d'une seance de cours</h1>

    @if(count($cours_r)!=0)
        <form method="post">
            <p>
                <label for="cours_id">Selectionenr le cours</label>
                <select name="cours_id">
                    @foreach($cours_r as $cours)
                        <option value="{{$cours->id}}">{{$cours->intitule}}</option>
                    @endforeach$
                </select>
            </p>

            <p>
                <label for="date_cours">Date du debut du cours</label>
                <input type="date" id="date_cours" name="date_cours" value="{{old('date_cours')}}">
            </p>

            <p>
                <label for="time_debut">Heure du debut du cours</label>
                <input type="time" id="time_debut" name="time_debut" value="{{old('time_debut')}}">
            </p>

            <p>
                <label for="time_fin">Heure du fin du cours</label>
                <input type="time" id="time_fin" name="time_fin" value="{{old('time_fin')}}">
            </p>
            <input type="submit" value="Envoyer">
            @csrf
        </form>
    @else
        <p><h3>Impossible de creer une seance de cours car vous n'êtes responsable d'aucun cours</h3></p>
    @endif

    @if($type=='enseignant')
        <p><h4><button><a href="{{route('enseignant.planning.cours.home_cours_ensei',['type'=>$type,'user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($type=='admin')

    @elseif($type=='enseignant_v2')
        <p><h4><button><a href="{{route('enseignant.planning.semaine.affichage_par_semaine_act_v2_ensei',['type'=>'enseignant','user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>
    @endif

@endsection

