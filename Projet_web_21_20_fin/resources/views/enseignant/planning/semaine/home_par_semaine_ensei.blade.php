@extends('modele')

@section('title','cours home')

@section('contents')
    <h1>Gestion du planning par cours</h1>
    <p>
        <ul>
            <p><li><a href="{{route('enseignant.planning.semaine.affichage_par_semaine_act_v2_ensei',['type'=>$type,'user_id'=>$user_id])}}">Creation d'une seance de cours</a> </li></p>
            <p><li><a href="{{route('enseignant.planning.semaine.affichage_par_semaine_act_v3_ensei',['type'=>$type,'user_id'=>$user_id])}}">MAJ/Supression d'une seance de cours </a> </li></p>
        </ul>
    </p>

    @if($type=='enseignant')
        <p><h4><button><a href="{{route('enseignant.planning.home_planning',['type'=>$type,'user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($type=='admin')

    @endif
@endsection
