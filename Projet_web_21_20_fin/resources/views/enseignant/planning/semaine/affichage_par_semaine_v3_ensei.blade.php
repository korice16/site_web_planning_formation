@extends('modele')

@section('title','affichage integrale')

@section('contents')
    <h1>Semaine du {{$st}} au {{$et}}</h1>
    <h3>
        <a href="{{route('enseignant.planning.semaine.affichage_par_semaine_v3_ensei',['type'=>$type,'user_id'=>$user_id,'page'=>'p','startweek'=>$st,'endweek'=>$et])}}">Semaine precedente</a>
        <a href="{{route('enseignant.planning.semaine.affichage_par_semaine_v3_ensei',['type'=>$type,'user_id'=>$user_id,'page'=>'s','startweek'=>$st,'endweek'=>$et])}}">Semaine suivante</a>
    </h3>
    @if($planning_e!=array())
        <table>
            <tr>
                <th>N°</th>
                <th>Nom</th>
                <th>Date debut</th>
                <th>Date fin</th>
                <th>Action</th>
            </tr>

            @foreach($planning_e as $planning)

                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$cours_e[$planning->cours_id]->intitule}}</td>
                    <td>{{$planning->date_debut}}</td>
                    <td>{{$planning->date_fin}}</td>
                    <td>
                        <a href="{{route('enseignant.planning.cours.maj_seance_cours_form',['type'=>'enseignant_v3','user_id'=>$user_id,'planning_id'=>$planning->id])}}">MAJ n°{{$loop->iteration}}</a>
                        / <a href="{{route('enseignant.planning.cours.supprimer_seance_cours',['type'=>'enseignant_v3','user_id'=>$user_id,'planning_id'=>$planning->id])}}">Supp n°{{$loop->iteration}}</a>
                    </td>
                </tr>
            @endforeach
        </table>

    @else
        <p><h3>Aucune seance de cours n'est disponible cette semainee</h3></p>
    @endif

    @if($type=='enseignant')
        <p><h4><button><a href="{{route('enseignant.planning.semaine.home_par_semaine_ensei',['type'=>$type,'user_id'=>$user_id])}}"><--- Retour en arriere</a> </button></h4></p>
    @elseif($type=='admin')

    @endif
@endsection
