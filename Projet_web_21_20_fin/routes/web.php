<?php

use App\Http\Controllers\AuthenticatedSessionController;
use App\Http\Controllers\CompteController;
use App\Http\Controllers\CoursController;
use App\Http\Controllers\FormationController;
use App\Http\Controllers\PlanningController;
use App\Http\Controllers\RegisterUserController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
})->name('main');
Route::view('/home','home')->middleware('auth')->name('home');

Route::view('/user/home','user.home')->middleware('auth')->name('home.user');
/*
Route::view('/admin','admin.home')->middleware('auth')
    ->middleware('is_admin')->name('admin.home');*/

//Routes pour se connecter et ce deconnecter

Route::get('/login', [AuthenticatedSessionController::class,'formLogin'])
    ->name('login');
Route::post('/login', [AuthenticatedSessionController::class,'login']);
Route::post('/modele/login', [AuthenticatedSessionController::class,'login']);
Route::get('/logout', [AuthenticatedSessionController::class,'logout'])
    ->name('logout')->middleware('auth');

//routes pour s'enregistrer
Route::get('/register', [RegisterUserController::class,'formRegister'])
    ->name('register');
Route::post('/register', [RegisterUserController::class,'register']);


    //--------------------------------------------------------------------------------------------------------------------------------

Route::middleware(['throttle:global','auth'])->group(function(){
    //Gestion du compte 3.1
    /*Route::middleware(['throttle:global','is_etudiant'])->group(function(){

    });*/
    Route::view('/compte/profile','compte.profile')->name('compte.profile');
    Route::get('/compte/profile/modifier_mdp',[CompteController::class,'modifier_mdp_form'])->name('compte.profile.modifier_mdp_form');
    Route::post('/compte/profile/modifier_mdp',[CompteController::class,'modifier_mdp']);

    Route::get('/compte/profile/modifier_nom_prenom',[CompteController::class,'modifier_nom_prenom_form'])->name('compte.profile.modifier_nom_prenom_form');
    Route::post('/compte/profile/modifier_nom_prenom',[CompteController::class,'modifier_nom_prenom']);


     //Partie administrateur

    Route::middleware(['throttle:is_admin','is_admin'])->group(function(){
        Route::view('/admin/home','admin.home_admin')->name('admin.home');//menu generale

        //Partie 4.1 gestion des utilisateurs
        Route::view('/admin/users','admin.users.users')->name('admin.users');

            //4.1.2. Acceptation (ou refus) d’un utilisateur qui a été auto-crée.
        Route::get('/admin/users/acceptation_refus_form',[UserController::class,'acceptation_refus_form'])->name('admin.users.acceptation_refus_form');
        //Route::get('/admin/users/acceptation_form/{user_id}',[UserController::class,'acceptation_form'])->name('admin.users.acceptation_form');
        Route::get('/admin/users/acceptation_form/{user_id}',[UserController::class,'acceptation'])->name('admin.users.acceptation');
        Route::get('/admin/users/refus/{user_id}',[UserController::class,'refus'])->name('admin.users.refus');

            //4.1.3. Association d’un enseignant à un cours (même point que 4.2.6).
        Route::get('/admin/{definir}/list_enseignant',[UserController::class,'list_enseignant'])->name('admin.users.list_enseignant');
        Route::get('/admin/{definir}/associer_enseignant_cours/{teacher_id}',[UserController::class,'associer_enseignant_cours_form'])->name('admin.users.associer_enseignant_cours_form');
        Route::get('/admin/{definir}/associer_enseignant_cours//{teacher_id}/{cours_id}',[UserController::class,'associer_enseignant_cours'])->name('admin.users.associer_enseignant_cours');

            //4.1.4. Création d’un utilisateur.
        Route::get('/admin/users/creation_utilisateur', [UserController::class,'creation_users_form'])->name('admin.users.creation_utilisateur');
        Route::post('/admin/users/creation_utilisateur', [UserController::class,'creation_users']);

            //4.1.5. Modification d’un utilisateur (y compris le type).
        Route::get('/admin/users/modifier_user/list_users',[UserController::class,'list_users_integrale'])->name('admin.users.modifier_user.list_users_integrale');

        Route::get('/admin/users/modifier_user/{user_id}',[UserController::class,'modifier_user_home'])->name('admin.users.modifier_user.modifier_user_home');
                    //modifier mdp
        Route::get('/admin/users/modifier_user/{user_id}/modifier_mdp',[UserController::class,'modifier_mdp_user_form'])->name('admin.users.modifier_user.modifier_mdp_user_form');
        Route::post('/admin/users/modifier_user/{user_id}/modifier_mdp',[UserController::class,'modifier_mdp_user']);
                    //modifier nom/prenom/login
        Route::get('/admin/users/modifier_user/{user_id}/modifier_nom_prenom_login',[UserController::class,'modifier_n_p_l_form'])->name('admin.users.modifier_user.modifier_n_p_l_form');
        Route::post('/admin/users/modifier_user/{user_id}/modifier_nom_prenom_login',[UserController::class,'modifier_n_p_l']);
                    //modifier type/formation
        Route::get('/admin/users/modifier_user/{user_id}/modifier_formation_type',[UserController::class,'modifier_formation_type_form'])->name('admin.users.modifier_user.modifier_formation_type_form');
        Route::post('/admin/users/modifier_user/{user_id}/modifier_formation_type',[UserController::class,'modifier_formation_type']);

            //4.1.6. Suppression d’un utilisateur.
        Route::get('/admin/users/{user_id}/suppression',[UserController::class,'suppression_user'])->name('admin.users.suppression');
            //4.1.1. Liste :
                //4.1.1.1. Intégrale. //4.1.1.2. Filtre par type (étudiant/enseignant).
        Route::get('/admin/users/liste/{filtre}',[UserController::class,'liste_filtre_inte_etu_ensei'])->name('admin.users.liste.liste_filtre_inte_etu_ensei');


        //Partie 4.2 gestion des cours
        Route::view('admin/cours','admin.cours.cours')->name('admin.cours');

            //4.2.1. Liste.
        Route::get('/admin/cours/cours_list',[CoursController::class,'cours_list'])->name('admin.cours.cours_list');

            //4.2.3. Création d’un cours.
        Route::get('/admin/cours/creation',[CoursController::class,'creation_form'])->name('admin.cours.creation_form');
        //Route::view('admin/cours/creation','admin.cours.creation_form')->name('admin.cours.creation_form');
        Route::post('admin/cours/creation',[CoursController::class,'creation'])->name('cours');

            //4.2.4. Modification d’un cours
        Route::get('/admin/cours/{cours_id}/modification',[CoursController::class,'modification_cours_form'])->name('admin.cours.modification');
        Route::post('/admin/cours/{cours_id}/modification',[CoursController::class,'modification_cours']);

            //4.2.5. Suppression d’un cours.
        Route::get('/admin/cours/{cours_id}/suppression',[CoursController::class,'suppression_cours'])->name('admin.cours.suppression');

            //4.2.7. Liste des cours par enseignant (similaire à 2.1, mais pour n’importe quel enseignant).
        Route::get('/admin/cours/list_cours_enseignant/{user_id}',[CoursController::class,'list_cours_enseignant'])->name('admin.cours.list_cours_enseignant');

        //Partie 4.3 Gestion des formations---------------------------------------------------------------------------------------------------------
        Route::view('admin/formations','admin.formations.formations')->name('admin.formations');

            //4.3.1 Liste
        Route::get('admin/formations/list_formations',[FormationController::class,'list_formations'])->name('admin.formations.list_formations');

            //4.3.2. Ajout d’une formation
        Route::view('admin/formations/ajouter','admin.formations.ajout_form')->name('admin.formations.ajout_form');
        Route::post('admin/formations/ajouter',[FormationController::class,'ajout'])->name('admin.formations.ajout');

            //4.3.3. Modification d’une formation.
        Route::get('admin/formations/modification/{formation_id}',[FormationController::class,'modification_form'])->name('admin.formations.modification');
        Route::post('admin/formations/modification/{formation_id}',[FormationController::class,'modification']);

            //4.3.4. Suppression d’une formation.
        Route::get('admin/formations/suppression/{formation_id}',[FormationController::class,'suppression'])->name('admin.formations.suppression');

        //4.4. Gestion des plannings :-----------------------------------------------------------------------------------------------------------

        Route::get('/admin/{definir}/list_enseigant/all',[UserController::class,'list_enseignant'])->name('admin.planning.list_enseignant');

            //2.3. Gestion du planning :
        Route::get('/{type}/{user_id}/planning/home/a',[PlanningController::class,'home_planning'])->name('admin.planning.home_planning');

            //Par Cours
        Route::get('/{type}/{user_id}/planning/par_cours/home_cours_enseignant/a',[PlanningController::class,'home_cours_ensei'])->name('admin.planning.cours.home_cours_ensei');

            //2.3.1. Création d’une nouvelle séance de cours.=>par cours

        //Route::view('/enseignant/planning/cours/creation_seance_cours','enseignant.planning.cours.creation_seance_cours_form')->name('enseignant.planning.cours.creation_seance_cours_form');
        Route::get('/{type}/{user_id}/planning/cours/creation_seance_cours/a',[PlanningController::class,'creation_seance_cours_form'])->name('admin.planning.cours.creation_seance_cours_form');
        Route::post('/{type}/{user_id}/planning/cours/creation_seance_cours/a',[PlanningController::class,'creation_seance_cours']);

        //2.3.2. Mise à jour d’une séance de cours.=>par cours
        Route::get('/{type}/{user_id}/planning/cours/list_maj_seance_cours/a',[PlanningController::class,'list_maj_seance_cours_form'])->name('admin.planning.cours.list_maj_seance_cours_form');
        Route::get('/{type}/{user_id}/planning/cours/maj_seance_cours/{planning_id}/a',[PlanningController::class,'maj_seance_cours_form'])->name('admin.planning.cours.maj_seance_cours_form');
        Route::post('/{type}/{user_id}/planning/cours/maj_seance_cours/{planning_id}/a',[PlanningController::class,'maj_seance_cours']);

        //2.3.3. Suppression d’une séance de cours=>par cours
        Route::get('/{type}/{user_id}/planning/cours/supprimer_seance_cours/{planning_id}/a',[PlanningController::class,'supprimer_seance_cours'])->name('admin.planning.cours.supprimer_seance_cours');

        //Par semaine
        Route::get('/{type}/{user_id}/planning/par_semaine/home_cours_enseignant/a',[PlanningController::class,'home_par_semaine_ensei'])->name('admin.planning.semaine.home_par_semaine_ensei');

        //2.3.1. Création d’une nouvelle séance de cours. =>Par semaine
        Route::get('/{type}/{user_id}/par_semaine/a',[PlanningController::class,'affichage_par_semaine_act_v2_ensei'])->name('admin.planning.semaine.affichage_par_semaine_act_v2_ensei');
        Route::get('/{type}/{user_id}/par_semaine/{page}/{startweek}/{endweek}/a',[PlanningController::class,'affichage_par_semaine_v2_ensei'])->name('admin.planning.semaine.affichage_par_semaine_v2_ensei');

        //2.3.2. Mise à jour d’une séance de cours. =>Par semaine
        //2.3.3. Suppression d’une séance de cours =>Par semaine
        Route::get('/{type}/{user_id}/v3/par_semaine/a',[PlanningController::class,'affichage_par_semaine_act_v3_ensei'])->name('admin.planning.semaine.affichage_par_semaine_act_v3_ensei');
        Route::get('/{type}/{user_id}/v3/par_semaine/{page}/{startweek}/{endweek}/a',[PlanningController::class,'affichage_par_semaine_v3_ensei'])->name('admin.planning.semaine.affichage_par_semaine_v3_ensei');

    });

    //========================================================================================================================================

    //Partie etudiants
    //Route::middleware(['is_etudiant'],['throttle:is_admin','is_admin'])->group(function(){
    Route::middleware('is_etudiant')->group(function(){
        Route::view('/etudiant/home','etudiant.home_etudiant')->name('etudiant.home_etudiant');

        //1.1 Voir la liste des cours de la formation (dans laquelle l’étudiant est inscrit).
        Route::get('/etudiant/list_cours/{but}',[CoursController::class,'list_cours_formation'])->name('etudiant.list_cours');

        //1.2. Gestion des inscriptions :
        Route::view('/etudiant/gestion_inscription','etudiant.inscriptions.home_inscription')->name('etudiant.home_inscription');

            //1.2.1. Inscription dans un cours
        Route::get('/etudiant/gestion_inscription/inscription',[CoursController::class,'inscritpion_form'])->name('etudiant.gestion_inscription.inscription');
        Route::post('/etudiant/gestion_inscription/inscription',[CoursController::class,'inscription']);

            //1.2.3. Liste des cours auxquels l’étudiant est inscrit
        Route::get('/etudiant/{type}/list_cours',[CoursController::class,'list_cours_inscrit'])->name('etudiant.gestion_inscription.list_cours');

            //1.2.2. Désinscription d’un cours.
        Route::get('/etudiant/gestion_inscription/desincription/{cours_id}',[CoursController::class,'desincription'])->name('etudiant.gestion_inscription.desincritpion');
        Route::view('/etudiant/gestion','etudiant.inscriptions.modele_search_li')->name('test');

        //Route::get('/etudiant/gestion_inscription/rechercher_cours_formation',[CoursController::class,'list_cours_formation'])->name('etudiant.gestion_inscription.rechercher_cours_formation');

        //1.3. Affichage du planning personnalisé (uniquement les séances des cours auxquels cet étudiant est inscrit) :
        Route::view('/etudiant/planning_perso/home_planning_perso','etudiant.planning_perso.home_planning_perso')->name('etudiant.planning_perso.home_planning_perso');

            //1.3.1. Intégral.
        Route::get('etudiant/planning_perso/affichage_integrale',[PlanningController::class,'affichage_integrale_etu'])->name('etudiant.planning_perso.affichage_integrale_etu');

            //1.3.2. Par cours.
        Route::get('/etudiant/{type}/list_cours_p',[CoursController::class,'list_cours_inscrit'])->name('etudiant.planning_perso.list_cours');
        Route::get('/etudiant/planning_perso/affichage_par_cours/{cours_id}',[PlanningController::class,'affichage_par_cours_etu'])->name('etudiant.planning_perso.affichage_par_cours_etu');

            //1.3.3. Par semaine.
        Route::get('/etudiant/planning_perso/par_semaine',[PlanningController::class,'affichage_par_semaine_act_etu'])->name('etudiant.planning_perso.affichage_par_semaine_act_etu');
        Route::get('/etudiant/planning_perso/par_semaine/{type}/{startweek}/{endweek}/',[PlanningController::class,'affichage_par_semaine_etu'])->name('etudiant.planning_perso.affichage_par_semaine_etu');



    });

    //==============================================================================================================================================

    //Partie enseignants
    Route::middleware('is_enseignant')->group(function(){
        Route::view('/enseignant/home','enseignant.home_enseignant')->name('enseignant.home_enseignant');

        //2.1. Voir la liste des cours dont on est responsable.
        Route::get('/enseignant/{type}',[CoursController::class,'list_cours_responsable'])->name('enseignant.cours.list_cours_responsable');

        //2.3. Gestion du planning :
        Route::get('/{type}/{user_id}/planning/home',[PlanningController::class,'home_planning'])->name('enseignant.planning.home_planning');

                //Par Cours
        Route::get('/{type}/{user_id}/planning/par_cours/home_cours_enseignant',[PlanningController::class,'home_cours_ensei'])->name('enseignant.planning.cours.home_cours_ensei');

        //2.3.1. Création d’une nouvelle séance de cours.=>par cours

        //Route::view('/enseignant/planning/cours/creation_seance_cours','enseignant.planning.cours.creation_seance_cours_form')->name('enseignant.planning.cours.creation_seance_cours_form');
        Route::get('/{type}/{user_id}/planning/cours/creation_seance_cours',[PlanningController::class,'creation_seance_cours_form'])->name('enseignant.planning.cours.creation_seance_cours_form');
        Route::post('/{type}/{user_id}/planning/cours/creation_seance_cours',[PlanningController::class,'creation_seance_cours']);

            //2.3.2. Mise à jour d’une séance de cours.=>par cours
        Route::get('/{type}/{user_id}/planning/cours/list_maj_seance_cours',[PlanningController::class,'list_maj_seance_cours_form'])->name('enseignant.planning.cours.list_maj_seance_cours_form');
        Route::get('/{type}/{user_id}/planning/cours/maj_seance_cours/{planning_id}',[PlanningController::class,'maj_seance_cours_form'])->name('enseignant.planning.cours.maj_seance_cours_form');
        Route::post('/{type}/{user_id}/planning/cours/maj_seance_cours/{planning_id}',[PlanningController::class,'maj_seance_cours']);

            //2.3.3. Suppression d’une séance de cours=>par cours
        Route::get('/{type}/{user_id}/planning/cours/supprimer_seance_cours/{planning_id}',[PlanningController::class,'supprimer_seance_cours'])->name('enseignant.planning.cours.supprimer_seance_cours');

                //Par semaine
        Route::get('/{type}/{user_id}/planning/par_semaine/home_cours_enseignant',[PlanningController::class,'home_par_semaine_ensei'])->name('enseignant.planning.semaine.home_par_semaine_ensei');

            //2.3.1. Création d’une nouvelle séance de cours. =>Par semaine
        Route::get('/{type}/{user_id}/par_semaine',[PlanningController::class,'affichage_par_semaine_act_v2_ensei'])->name('enseignant.planning.semaine.affichage_par_semaine_act_v2_ensei');
        Route::get('/{type}/{user_id}/par_semaine/{page}/{startweek}/{endweek}',[PlanningController::class,'affichage_par_semaine_v2_ensei'])->name('enseignant.planning.semaine.affichage_par_semaine_v2_ensei');

            //2.3.2. Mise à jour d’une séance de cours. =>Par semaine
        //2.3.3. Suppression d’une séance de cours =>Par semaine
        Route::get('/{type}/{user_id}/v3/par_semaine',[PlanningController::class,'affichage_par_semaine_act_v3_ensei'])->name('enseignant.planning.semaine.affichage_par_semaine_act_v3_ensei');
        Route::get('/{type}/{user_id}/v3/par_semaine/{page}/{startweek}/{endweek}',[PlanningController::class,'affichage_par_semaine_v3_ensei'])->name('enseignant.planning.semaine.affichage_par_semaine_v3_ensei');

        //2.2. Voir le planning personnalisé (les séances dont on est responsable) :
        Route::view('/enseignant/planning_perso/home_planning_perso','enseignant.planning_perso.home_planning_perso')->name('enseignant.planning_perso.home_planning_perso');

            //2.2.1. Intégral.
        Route::get('/enseignant/planning_perso/affichage_integrale',[PlanningController::class,'affichage_integrale_ensei'])->name('enseignant.planning_perso.affichage_integrale_ensei');

            //2.2.2. Par cours.
        Route::get('/enseignant/{type}/list_cours_responsable',[CoursController::class,'list_cours_responsable'])->name('enseignant.planning_perso.list_cours_responsable');
        Route::get('/enseignant/planning_perso/affichage_par_cours/{cours_id}',[PlanningController::class,'affichage_par_cours_ensei'])->name('enseignant.planning_perso.affichage_par_cours_ensei');

            //2.2.3. Par semaine
        Route::get('/enseignant/planning_perso/par_semaine/{user_id}',[PlanningController::class,'affichage_par_semaine_act_ensei'])->name('enseignant.planning_perso.affichage_par_semaine_act_ensei');
        Route::get('/enseignant/planning_perso/par_semaine/{user_id}/{type}/{startweek}/{endweek}',[PlanningController::class,'affichage_par_semaine_ensei'])->name('enseignant.planning_perso.affichage_par_semaine_ensei');

    });
});
